package utilities

import (
	"strings"
	"testing"
)

func TestAsToIs(t *testing.T) {
	var tests = []struct {
		strs  []string
		ints  []int
		error bool
	}{
		{
			[]string{"16", "10", "15", "5", "1", "11", "7", "19", "6", "12", "4"},
			[]int{16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4},
			false,
		},
		{
			[]string{"28", "33", "18", "42", "31", "14", "46", "20", "48", "47", "24", "23", "49", "45", "19", "38", "39", "11", "1", "32", "25", "35", "8", "17", "7", "9", "4", "2", "34", "10", "3"},
			[]int{28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3},
			false,
		},
		{
			[]string{"16", "10", "15", "5.45", "1", "11", "7", "19", "6", "12", "4"},
			nil,
			true,
		},
		{
			[]string{"16", "10", "15", "5", "1a", "11", "7", "19", "6", "12", "4"},
			nil,
			true,
		},
	}
	for _, test := range tests {
		t.Run(strings.Join(test.strs, ","), func(t *testing.T) {
			expected := test.ints
			actual, err := AsToIs(test.strs)
			if !test.error {
				if err != nil {
					t.Error(err)
				}
				if len(actual) != len(expected) {
					t.Fatalf("expected len: %d, actual len: %d", len(expected), len(actual))
				}
				for i, val := range actual {
					if val != expected[i] {
						t.Fatalf("expected: %v, actual %v", expected, actual)
					}
				}
			} else {
				if err == nil {
					t.Fatal("expected an error, but none occurred")
				}
				if actual != nil {
					t.Fatalf("expected 'is' to be null when error occurs, but was: %v", actual)
				}
			}
		})
	}
}
