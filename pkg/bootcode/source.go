package bootcode

import "fmt"

type Command int

const (
	acc = iota
	nop = iota
	jmp = iota
)

func NewCommand(val string) Command {
	return map[string]Command{
		"acc": acc,
		"nop": nop,
		"jmp": jmp,
	}[val]
}

func (command Command) String() string {
	return map[Command]string{
		acc: "acc",
		nop: "nop",
		jmp: "jmp",
	}[command]
}

type Line struct {
	cmd Command
	val int
}

func (line Line) String() string {
	return fmt.Sprintf("%s %d", line.cmd.String(), line.val)
}

type Source []Line

func (source Source) String() string {
	str := ""
	for _, item := range source {
		str += item.String() + "\n"
	}
	return str
}
