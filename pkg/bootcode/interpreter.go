package bootcode

import (
	"errors"
	"strconv"
	"strings"
)

type Interpreter struct {
	accumulator int
	source      Source
	index       int
	executed    []int
}

func NewInterpreter(source []string) (*Interpreter, error) {
	code := &Interpreter{
		accumulator: 0,
		source:      Source{},
		index:       0,
		executed:    []int{},
	}
	for _, line := range source {
		err := code.addLine(line)
		if err != nil {
			return nil, err
		}
	}
	return code, nil
}

func (code *Interpreter) addLine(line string) error {
	parts := strings.Split(line, " ")
	val, err := strconv.Atoi(parts[1])
	if err != nil {
		return err
	}
	code.source = append(code.source, Line{
		cmd: NewCommand(parts[0]),
		val: val,
	})

	return nil
}

func (code *Interpreter) Execute() (int, error) {
	for {
		for _, val := range code.executed {
			if val == code.index {
				return code.accumulator, errors.New("executed command twice")
			}
		}
		if code.index == len(code.source) {
			return code.accumulator, nil
		}
		command := code.source[code.index]
		code.executed = append(code.executed, code.index)
		switch command.cmd {
		case jmp:
			code.index += command.val
		case acc:
			code.accumulator += command.val
			fallthrough
		case nop:
			code.index += 1
		}
	}
}
