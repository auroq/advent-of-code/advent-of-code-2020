package part2

import (
	"strconv"
	"strings"
)

type Part2 struct{}

func (day Part2) Run(input []string) (answer int, err error) {
	for _, line := range input {
		min, max, char, pass, err := parseLine(line)
		if err != nil {
			return -1, err
		}
		if isValid(min, max, char, pass) {
			answer++
		}
	}
	return
}

func isValid(pos1, pos2 int, char rune, pass string) bool {
	return (rune(pass[pos1-1]) == char) != (rune(pass[pos2-1]) == char)
}

func parseLine(line string) (pos1 int, pos2 int, char rune, pass string, err error) {
	parts := strings.Split(line, " ")
	nums := strings.Split(parts[0], "-")
	pos1, err = strconv.Atoi(nums[0])
	if err != nil {
		return
	}
	pos2, err = strconv.Atoi(nums[1])
	if err != nil {
		return
	}
	char = rune(parts[1][0])

	pass = parts[2]

	return
}
