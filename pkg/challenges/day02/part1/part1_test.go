package part1

import (
	"strconv"
	"testing"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"1-3 a: abcde",
		"1-3 b: cdefg",
		"2-9 c: ccccccccc",
	}
	expected := 2
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestIsValid(t *testing.T) {
	var tests = []struct {
		min   int
		max   int
		char  rune
		pass  string
		valid bool
	}{
		{1, 3, 'a', "abcde", true},
		{1, 3, 'b', "cdefg", false},
		{2, 9, 'c', "ccccccccc", true},
	}
	for i, test := range tests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			expected := test.valid
			actual := isValid(test.min, test.max, test.char, test.pass)
			if actual != expected {
				t.Fatalf("expected: %t, actual: %t", expected, actual)
			}
		})
	}
}

func TestParseLine(t *testing.T) {

	var tests = []struct {
		line string
		min  int
		max  int
		char rune
		pass string
	}{
		{"1-3 a: abcde", 1, 3, 'a', "abcde"},
		{"1-3 b: cdefg", 1, 3, 'b', "cdefg"},
		{"2-9 c: ccccccccc", 2, 9, 'c', "ccccccccc"},
	}
	for i, test := range tests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			amin, amax, achar, apass, err := parseLine(test.line)
			if err != nil {
				t.Error(err)
			}
			if amin != test.min {
				t.Fatalf("did not match")
			}
			if amax != test.max {
				t.Fatalf("did not match")
			}
			if achar != test.char {
				t.Fatalf("did not match")
			}
			if apass != test.pass {
				t.Fatalf("did not match")
			}
		})
	}
}
