package part1

import (
	"strconv"
	"strings"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	for _, line := range input {
		min, max, char, pass, err := parseLine(line)
		if err != nil {
			return -1, err
		}
		if isValid(min, max, char, pass) {
			answer++
		}
	}
	return
}

func isValid(min, max int, char rune, pass string) bool {
	count := 0
	for _, c := range pass {
		if c == char {
			count++
		}
		if count > max {
			return false
		}
	}
	return count >= min
}

func parseLine(line string) (min int, max int, char rune, pass string, err error) {
	parts := strings.Split(line, " ")
	nums := strings.Split(parts[0], "-")
	min, err = strconv.Atoi(nums[0])
	if err != nil {
		return
	}
	max, err = strconv.Atoi(nums[1])
	if err != nil {
		return
	}
	char = rune(parts[1][0])

	pass = parts[2]

	return
}
