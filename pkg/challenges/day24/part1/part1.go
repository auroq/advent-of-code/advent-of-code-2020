package part1

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/challenges/day24/hotel"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	floor := hotel.NewFloor()
	for _, line := range input {
		err = floor.FlipTile(line)
		if err != nil {
			return
		}
	}

	answer = floor.CountBlackTiles()

	return
}
