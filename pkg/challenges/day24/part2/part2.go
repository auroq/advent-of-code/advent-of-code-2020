package part2

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/challenges/day24/hotel"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	floor := hotel.NewFloor()
	for _, line := range input {
		err = floor.FlipTile(line)
		if err != nil {
			return
		}
	}

	for i := 0; i < 100; i++ {
		floor = floor.Iterate()
	}
	answer = floor.CountBlackTiles()

	return
}
