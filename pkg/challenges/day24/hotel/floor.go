package hotel

import (
	"fmt"
	"math"
	"strings"
)

type (
	Color     string
	Direction string

	Position struct {
		X, Y int
	}

	Floor struct {
		Tiles       map[Position]*Tile
		maxPosition Position
		minPosition Position
	}

	Tile struct{}
)

const (
	NorthWest Direction = "NorthWest"
	NorthEast Direction = "NorthEast"
	West      Direction = "West"
	East      Direction = "East"
	SouthWest Direction = "SouthWest"
	SouthEast Direction = "SouthEast"
)

func NewDirection(direction string) (Direction, bool) {
	val, ok := map[string]Direction{
		"nw": NorthWest,
		"ne": NorthEast,
		"w":  West,
		"e":  East,
		"sw": SouthWest,
		"se": SouthEast,
	}[strings.ToLower(direction)]

	return val, ok
}

func NewTile() *Tile {
	return &Tile{}
}

func NewFloor() *Floor {
	return &Floor{
		Tiles:       map[Position]*Tile{},
		maxPosition: Position{0, 0},
		minPosition: Position{0, 0},
	}
}

func (floor *Floor) FlipTile(directionsStr string) error {
	var directions []Direction
	for i := 0; i < len(directionsStr); i++ {
		if val, ok := NewDirection(directionsStr[i : i+1]); ok {
			directions = append(directions, val)
		} else if val, ok = NewDirection(directionsStr[i : i+2]); ok {
			directions = append(directions, val)
			i++
		} else {
			return fmt.Errorf("unable to parse string '%s' at character %d", directionsStr, i)
		}
	}

	position := Position{0, 0}
	for _, direction := range directions {
		var err error
		position, err = position.Next(direction)
		if err != nil {
			return err
		}
	}

	floor.ToggleTile(position)

	return nil
}

func (floor *Floor) ToggleTile(position Position) {
	if _, ok := floor.Tiles[position]; ok {
		delete(floor.Tiles, position)
	} else {
		floor.Tiles[position] = NewTile()
		if position.X > floor.maxPosition.X {
			floor.maxPosition.X = position.X
		}
		if position.Y > floor.maxPosition.Y {
			floor.maxPosition.Y = position.Y
		}
		if position.X < floor.minPosition.X {
			floor.minPosition.X = position.X
		}
		if position.Y < floor.minPosition.Y {
			floor.minPosition.Y = position.Y
		}
	}
}

func (position Position) Next(direction Direction) (Position, error) {
	switch direction {
	case NorthWest:
		return Position{position.X - 1, position.Y - 1}, nil
	case NorthEast:
		return Position{position.X + 1, position.Y - 1}, nil
	case West:
		return Position{position.X - 2, position.Y}, nil
	case East:
		return Position{position.X + 2, position.Y}, nil
	case SouthWest:
		return Position{position.X - 1, position.Y + 1}, nil
	case SouthEast:
		return Position{position.X + 1, position.Y + 1}, nil
	}
	return position, fmt.Errorf("unknown direction %s", direction)
}

func (floor Floor) CountBlackTiles() int {
	return len(floor.Tiles)
}

func (position Position) String() string {
	return fmt.Sprintf("(%d, %d)", position.X, position.Y)
}

func BootStrapFloor(input []string) (floor *Floor, err error) {
	floor = NewFloor()
	for _, line := range input {
		err = floor.FlipTile(line)
		if err != nil {
			return
		}
	}

	return
}

func (floor Floor) Iterate() (newFloor *Floor) {
	newFloor = NewFloor()
	// Arbitrarily picked 500 after it seemed to be big enough to make the tests pass
	for y := -500; y <= 500; y++ {
		startingX := -500
		if int(math.Abs(float64(y)))%2 != 0 {
			startingX++
		}
		for x := startingX; x <= 500; x += 2 {
			position := Position{X: x, Y: y}
			adjacentCount := floor.CountAdjacent(position)
			if _, found := floor.Tiles[position]; found {
				if adjacentCount > 0 && adjacentCount <= 2 {
					newFloor.ToggleTile(position)
				}
			} else {
				if adjacentCount == 2 {
					newFloor.ToggleTile(position)
				}
			}
		}
	}

	return
}

func (floor Floor) CountAdjacent(position Position) (count int) {
	count = 0
	if _, ok := floor.Tiles[Position{position.X - 1, position.Y - 1}]; ok {
		count++
	}
	if _, ok := floor.Tiles[Position{position.X + 1, position.Y - 1}]; ok {
		count++
	}
	if _, ok := floor.Tiles[Position{position.X - 2, position.Y}]; ok {
		count++
	}
	if _, ok := floor.Tiles[Position{position.X + 2, position.Y}]; ok {
		count++
	}
	if _, ok := floor.Tiles[Position{position.X - 1, position.Y + 1}]; ok {
		count++
	}
	if _, ok := floor.Tiles[Position{position.X + 1, position.Y + 1}]; ok {
		count++
	}

	return
}

func (floor Floor) Print() {
	fmt.Println(" 098765432101234567890")
	for y := -500; y <= 500; y++ {
		fmt.Print(math.Abs(float64(y % 10)))
		startingX := -500
		if int(math.Abs(float64(y)))%2 != 0 {
			startingX++
			fmt.Print(".")
		}
		for x := startingX; x <= 500; x += 2 {
			position := Position{X: x, Y: y}
			if _, ok := floor.Tiles[position]; ok {
				if position.X == 0 && position.Y == 0 {
					fmt.Print("■.")
				} else {
					//fmt.Print("⬢.")
					fmt.Print("█.")
				}
			} else {
				if position.X == 0 && position.Y == 0 {
					fmt.Print("□.")
				} else {
					//fmt.Print("⬡.")
					fmt.Print("░.")
				}
			}
		}
		fmt.Println()
	}
	fmt.Println()
}
