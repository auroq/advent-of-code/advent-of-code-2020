package hotel_test

import (
	"fmt"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/challenges/day24/hotel"
	"testing"
)

func TestFloor_FlipTile(t *testing.T) {
	var tests = []struct {
		directionsStr     string
		expectedPositions []hotel.Position
	}{
		{
			"esew",
			[]hotel.Position{{1, 1}},
		}, {
			"nwwswee",
			[]hotel.Position{{0, 0}},
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.directionsStr, func(t *testing.T) {
			floor := hotel.NewFloor()
			err := floor.FlipTile(test.directionsStr)
			if err != nil {
				t.Error(err)
			}
			for _, expected := range test.expectedPositions {
				if _, ok := floor.Tiles[expected]; !ok {
					t.Errorf("position expected: %s but was not found", expected)
				}
			}
			if len(floor.Tiles) != len(test.expectedPositions) {
				t.Errorf("length expected: %d, actual: %d", len(test.expectedPositions), len(floor.Tiles))
			}
		})
	}
}

func TestFloor_CountBlackTiles(t *testing.T) {
	t.Run("when floor is empty", func(t *testing.T) {
		expected := 0
		actual := hotel.NewFloor().CountBlackTiles()
		if actual != expected {
			t.Fatalf("expected: %d, actual: %d", expected, actual)
		}
	})
	t.Run("when contains tiles", func(t *testing.T) {
		floor := hotel.NewFloor()
		_ = floor.FlipTile("nw")
		_ = floor.FlipTile("ne")
		_ = floor.FlipTile("w")
		_ = floor.FlipTile("e")
		_ = floor.FlipTile("sw")
		_ = floor.FlipTile("se")
		expected := 6
		actual := floor.CountBlackTiles()
		if actual != expected {
			t.Fatalf("expected: %d, actual: %d", expected, actual)
		}
	})
}

func TestFloor_Iterate(t *testing.T) {
	var tests = []struct {
		day   int
		count int
	}{
		{day: 0, count: 10},
		{day: 1, count: 15},
		{day: 2, count: 12},
		{day: 3, count: 25},
		{day: 4, count: 14},
		{day: 5, count: 23},
		{day: 6, count: 28},
		{day: 7, count: 41},
		{day: 8, count: 37},
		{day: 9, count: 49},
		{day: 10, count: 37},
		{day: 20, count: 132},
		{day: 30, count: 259},
		{day: 40, count: 406},
		{day: 50, count: 566},
		{day: 60, count: 788},
		{day: 70, count: 1106},
		{day: 80, count: 1373},
		{day: 90, count: 1844},
		{day: 100, count: 2208},
	}
	for _, test := range tests {
		test := test
		t.Run(fmt.Sprintf("Day: %d Count: %d", test.day, test.count), func(t *testing.T) {
			t.Parallel()
			floor, err := hotel.BootStrapFloor([]string{
				"sesenwnenenewseeswwswswwnenewsewsw",
				"neeenesenwnwwswnenewnwwsewnenwseswesw",
				"seswneswswsenwwnwse",
				"nwnwneseeswswnenewneswwnewseswneseene",
				"swweswneswnenwsewnwneneseenw",
				"eesenwseswswnenwswnwnwsewwnwsene",
				"sewnenenenesenwsewnenwwwse",
				"wenwwweseeeweswwwnwwe",
				"wsweesenenewnwwnwsenewsenwwsesesenwne",
				"neeswseenwwswnwswswnw",
				"nenwswwsewswnenenewsenwsenwnesesenew",
				"enewnwewneswsewnwswenweswnenwsenwsw",
				"sweneswneswneneenwnewenewwneswswnese",
				"swwesenesewenwneswnwwneseswwne",
				"enesenwswwswneneswsenwnewswseenwsese",
				"wnwnesenesenenwwnenwsewesewsesesew",
				"nenewswnwewswnenesenwnesewesw",
				"eneswnwswnwsenenwnwnwwseeswneewsenese",
				"neswnwewnwnwseenwseesewsenwsweewe",
				"wseweeenwnesenwwwswnew",
			})
			if err != nil {
				t.Fatal(err)
			}
			for i := test.day; i > 0; i-- {
				floor = floor.Iterate()
			}
			expected := test.count
			actual := floor.CountBlackTiles()
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}

func TestFloor_CountAdjacent(t *testing.T) {
	var tests = []struct {
		tiles         []hotel.Position
		adjacentCount int
	}{
		{
			tiles: []hotel.Position{}, adjacentCount: 0,
		},
		{
			tiles: []hotel.Position{
				{-2, 0},
			}, adjacentCount: 1,
		},
		{
			tiles: []hotel.Position{
				{-2, 0},
				{2, 0},
			}, adjacentCount: 2,
		},
		{
			tiles: []hotel.Position{
				{-1, -1},
				{2, 0},
				{-1, 1},
			}, adjacentCount: 3,
		},
		{
			tiles: []hotel.Position{
				{-1, -1},
				{-1, 1},
				{1, -1},
				{1, 1},
			}, adjacentCount: 4,
		},
		{
			tiles: []hotel.Position{
				{-1, -1},
				{-1, 1},
				{1, -1},
				{1, 1},
				{-2, 0},
			}, adjacentCount: 5,
		},
		{
			tiles: []hotel.Position{
				{-1, -1},
				{-1, 1},
				{1, -1},
				{1, 1},
				{-2, 0},
				{2, 0},
			}, adjacentCount: 6,
		},
		{
			tiles: []hotel.Position{
				{0, -2},
				{-2, -2},
				{2, -2},
				{-3, -1},
				{3, -1},
				{-4, 0},
				{4, 0},
				{-3, 1},
				{3, 1},
				{-2, 2},
				{2, 2},
				{0, 2},
			}, adjacentCount: 0,
		},
	}
	for _, test := range tests {
		test := test
		t.Run(fmt.Sprintf("count: %d adjacent: %d", len(test.tiles), test.adjacentCount), func(t *testing.T) {
			t.Parallel()
			floor := hotel.NewFloor()
			for _, position := range test.tiles {
				floor.ToggleTile(position)
			}
			expected := test.adjacentCount
			actual := floor.CountAdjacent(hotel.Position{X: 0, Y: 0})
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}
