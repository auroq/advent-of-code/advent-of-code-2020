package part2

import (
	"fmt"
	"math"
	"strconv"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	ship := NewShip()
	err = ship.Travel(input)
	answer = int(math.Abs(float64(ship.X))) + int(math.Abs(float64(ship.Y)))
	return
}

const (
	North CardinalDirection = iota
	South
	East
	West
)

const (
	Right Direction = iota
	Left
)

type (
	Direction         int
	CardinalDirection int

	Rotation struct {
		Direction Direction
		Degrees   int
	}

	SeaItem struct {
		X int
		Y int
	}

	Ship struct {
		*SeaItem
		Waypoint *Waypoint
	}

	Waypoint struct {
		*SeaItem
		Ship *Ship
	}
)

func (cardinal CardinalDirection) String() string {
	return map[CardinalDirection]string{
		North: "North",
		South: "South",
		East:  "East",
		West:  "West",
	}[cardinal]
}

func (direction Direction) String() string {
	return [...]string{
		Left:  "Left",
		Right: "Right",
	}[direction]
}

func (rotation Rotation) String() string {
	return fmt.Sprintf("%s %d%%", rotation.Direction.String(), rotation.Degrees)
}

func NewShip() *Ship {
	ship := &Ship{
		&SeaItem{
			X: 0,
			Y: 0,
		},
		nil,
	}
	ship.Waypoint = NewWaypoint(ship)
	return ship
}

func NewWaypoint(ship *Ship) *Waypoint {
	return &Waypoint{
		&SeaItem{
			X: 10,
			Y: 1,
		},
		ship,
	}
}

func (waypoint *Waypoint) Rotate(rotation Rotation) {
	rotations := rotation.Degrees / 90

	for i := 0; i < rotations; i++ {
		x, y := waypoint.RelativePosition()
		var relX, relY int
		switch rotation.Direction {
		case Right:
			relX = y
			relY = x * -1
		case Left:
			relY = x
			relX = y * -1
		}
		waypoint.SetPositionFromRelative(relX, relY)
	}
}

func (waypoint Waypoint) RelativePosition() (x, y int) {
	return waypoint.X - waypoint.Ship.X, waypoint.Y - waypoint.Ship.Y
}

func (waypoint *Waypoint) SetPositionFromRelative(x, y int) {
	waypoint.X = waypoint.Ship.X + x
	waypoint.Y = waypoint.Ship.Y + y
}

func (waypoint *Waypoint) Move(direction CardinalDirection, amount int) {
	switch direction {
	case North:
		waypoint.Y += amount
	case East:
		waypoint.X += amount
	case South:
		waypoint.Y -= amount
	case West:
		waypoint.X -= amount
	}
}

func (ship *Ship) Forward(amount int) {
	relX, relY := ship.Waypoint.RelativePosition()
	for i := 0; i < amount; i++ {
		ship.X += relX
		ship.Y += relY
	}
	ship.Waypoint.X = ship.X + relX
	ship.Waypoint.Y = ship.Y + relY
}

func (ship *Ship) Travel(instructions []string) error {
	for _, instruction := range instructions {
		action := instruction[0:1]
		amount, err := strconv.Atoi(instruction[1:])
		if err != nil {
			return err
		}
		switch action {
		case "N":
			ship.Waypoint.Move(North, amount)
		case "S":
			ship.Waypoint.Move(South, amount)
		case "E":
			ship.Waypoint.Move(East, amount)
		case "W":
			ship.Waypoint.Move(West, amount)
		case "L":
			ship.Waypoint.Rotate(Rotation{Left, amount})
		case "R":
			ship.Waypoint.Rotate(Rotation{Right, amount})
		case "F":
			ship.Forward(amount)
		}
	}

	return nil
}
