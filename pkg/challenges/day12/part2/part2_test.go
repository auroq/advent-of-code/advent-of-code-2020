package part2

import (
	"strconv"
	"testing"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{
		"F10",
		"N3",
		"F7",
		"R90",
		"F11",
	}
	expected := 286
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestNewShip(t *testing.T) {
	ship := NewShip()
	if ship.X != 0 {
		t.Fatalf("X expected: %d, actual: %d", 0, ship.X)
	}
	if ship.Y != 0 {
		t.Fatalf("Y expected: %d, actual: %d", 0, ship.Y)
	}
}

func TestNewWaypoint(t *testing.T) {
	waypoint := NewWaypoint(nil)
	if waypoint.X != 10 {
		t.Fatalf("X expected: %d, actual: %d", 10, waypoint.X)
	}
	if waypoint.Y != 1 {
		t.Fatalf("Y expected: %d, actual: %d", 1, waypoint.Y)
	}
}

func TestWaypoint_Rotate(t *testing.T) {
	var tests = []struct {
		startX    int
		startY    int
		rotation  Rotation
		expectedX int
		expectedY int
	}{
		{0, 1, Rotation{Right, 90}, 1, 0},
		{1, 0, Rotation{Right, 90}, 0, -1},
		{0, -1, Rotation{Right, 90}, -1, 0},
		{-1, 0, Rotation{Right, 90}, 0, 1},

		{0, 1, Rotation{Right, 180}, 0, -1},
		{1, 0, Rotation{Right, 180}, -1, 0},
		{0, -1, Rotation{Right, 180}, 0, 1},
		{-1, 0, Rotation{Right, 180}, 1, 0},

		{0, 1, Rotation{Right, 270}, -1, 0},
		{1, 0, Rotation{Right, 270}, 0, 1},
		{0, -1, Rotation{Right, 270}, 1, 0},
		{-1, 0, Rotation{Right, 270}, 0, -1},

		{0, 1, Rotation{Right, 360}, 0, 1},
		{1, 0, Rotation{Right, 360}, 1, 0},
		{0, -1, Rotation{Right, 360}, 0, -1},
		{-1, 0, Rotation{Right, 360}, -1, 0},

		{0, 1, Rotation{Left, 90}, -1, 0},
		{1, 0, Rotation{Left, 90}, 0, 1},
		{0, -1, Rotation{Left, 90}, 1, 0},
		{-1, 0, Rotation{Left, 90}, 0, -1},

		{0, 1, Rotation{Left, 180}, 0, -1},
		{1, 0, Rotation{Left, 180}, -1, 0},
		{0, -1, Rotation{Left, 180}, 0, 1},
		{-1, 0, Rotation{Left, 180}, 1, 0},

		{0, 1, Rotation{Left, 270}, 1, 0},
		{1, 0, Rotation{Left, 270}, 0, -1},
		{0, -1, Rotation{Left, 270}, -1, 0},
		{-1, 0, Rotation{Left, 270}, 0, 1},

		{0, 1, Rotation{Left, 360}, 0, 1},
		{1, 0, Rotation{Left, 360}, 1, 0},
		{0, -1, Rotation{Left, 360}, 0, -1},
		{-1, 0, Rotation{Left, 360}, -1, 0},
	}

	for i, test := range tests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			ship := NewShip()
			ship.Waypoint.X = test.startX
			ship.Waypoint.Y = test.startY
			ship.Waypoint.Rotate(test.rotation)

			if ship.Waypoint.X != test.expectedX {
				t.Fatalf("X expected: %d, actual: %d", test.expectedX, ship.Waypoint.X)
			}
			if ship.Waypoint.Y != test.expectedY {
				t.Fatalf("Y expected: %d, actual: %d", test.expectedY, ship.Waypoint.Y)
			}
		})
	}
}

func TestWaypoint_Move(t *testing.T) {
	movementValue := 10
	var tests = []struct {
		direction CardinalDirection
		expectedX int
		expectedY int
	}{
		{North, 0, movementValue},
		{East, movementValue, 0},
		{South, 0, -movementValue},
		{West, -movementValue, 0},
	}

	for _, test := range tests {
		t.Run(test.direction.String(), func(t *testing.T) {
			waypoint := NewWaypoint(nil)
			waypoint.X = 0
			waypoint.Y = 0
			waypoint.Move(test.direction, movementValue)

			if waypoint.X != test.expectedX {
				t.Fatalf("X expected: %d, actual: %d", test.expectedX, waypoint.X)
			}
			if waypoint.Y != test.expectedY {
				t.Fatalf("Y expected: %d, actual: %d", test.expectedY, waypoint.Y)
			}
		})
	}
}

func TestShip_Forward(t *testing.T) {
	movementValue := 10
	var tests = []struct {
		WaypointX   int
		WaypointY   int
		expectedX   int
		expectedY   int
		expectedWpX int
		expectedWpY int
	}{
		{0, 1, 0, movementValue, 0, movementValue + 1},
		{1, 0, movementValue, 0, movementValue + 1, 0},
		{0, -1, 0, -movementValue, 0, -movementValue - 1},
		{-1, 0, -movementValue, 0, -movementValue - 1, 0},
	}

	for i, test := range tests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			ship := NewShip()
			ship.Waypoint.X = test.WaypointX
			ship.Waypoint.Y = test.WaypointY
			ship.Forward(movementValue)

			if ship.X != test.expectedX {
				t.Fatalf("X expected: %d, actual: %d", test.expectedX, ship.X)
			}
			if ship.Y != test.expectedY {
				t.Fatalf("Y expected: %d, actual: %d", test.expectedY, ship.Y)
			}

			if ship.Waypoint.X != test.expectedWpX {
				t.Fatalf("X expected: %d, actual: %d", test.expectedWpX, ship.Waypoint.X)
			}
			if ship.Waypoint.Y != test.expectedWpY {
				t.Fatalf("Y expected: %d, actual: %d", test.expectedWpY, ship.Waypoint.Y)
			}
		})
	}
}

func TestShip_Travel(t *testing.T) {
	var instructions = []string{
		"F10",
		"N3",
		"F7",
		"R90",
		"F11",
	}
	ship := NewShip()
	err := ship.Travel(instructions)
	expectedX := 214
	expectedY := -72
	if err != nil {
		t.Error(err)
	}
	if ship.X != expectedX {
		t.Fatalf("X expected: %d, actual: %d", expectedX, ship.X)
	}
	if ship.Y != expectedY {
		t.Fatalf("Y expected: %d, actual: %d", expectedY, ship.Y)
	}
}
