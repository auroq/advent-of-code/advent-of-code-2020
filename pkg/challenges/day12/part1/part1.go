package part1

import (
	"fmt"
	"math"
	"strconv"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	ship := NewShip()
	err = ship.Travel(input)
	answer = int(math.Abs(float64(ship.X))) + int(math.Abs(float64(ship.Y)))
	return
}

const (
	North CardinalDirection = iota
	South
	East
	West
)

const (
	Right Direction = iota
	Left
)

type (
	Direction         int
	CardinalDirection int

	Rotation struct {
		Direction Direction
		Degrees   int
	}

	Ship struct {
		CardinalDirection CardinalDirection
		X                 int
		Y                 int
	}
)

func (cardinal CardinalDirection) String() string {
	return map[CardinalDirection]string{
		North: "North",
		South: "South",
		East:  "East",
		West:  "West",
	}[cardinal]
}

func (direction Direction) String() string {
	return [...]string{
		Left:  "Left",
		Right: "Right",
	}[direction]
}

func (rotation Rotation) String() string {
	return fmt.Sprintf("%s %d%%", rotation.Direction.String(), rotation.Degrees)
}

func NewShip() *Ship {
	return &Ship{
		CardinalDirection: East,
		X:                 0,
		Y:                 0,
	}
}

func (ship *Ship) Rotate(rotation Rotation) {
	getStartingIndex := func(slice []CardinalDirection) int {
		for i, val := range slice {
			if val == ship.CardinalDirection {
				return i
			}
		}
		return -1
	}
	clockwise := []CardinalDirection{North, East, South, West}
	counter := []CardinalDirection{North, West, South, East}
	rotations := rotation.Degrees / 90
	var newDirection CardinalDirection
	switch rotation.Direction {
	case Right:
		startingIndex := getStartingIndex(clockwise)
		newDirection = clockwise[(startingIndex+rotations)%len(clockwise)]
	case Left:
		startingIndex := getStartingIndex(counter)
		newDirection = counter[(startingIndex+rotations)%len(counter)]
	}
	ship.CardinalDirection = newDirection
}

func (ship *Ship) Move(direction CardinalDirection, amount int) {
	switch direction {
	case North:
		ship.Y += amount
	case East:
		ship.X += amount
	case South:
		ship.Y -= amount
	case West:
		ship.X -= amount
	}
}

func (ship *Ship) Forward(amount int) {
	ship.Move(ship.CardinalDirection, amount)
}

func (ship *Ship) Travel(instructions []string) error {
	for _, instruction := range instructions {
		action := instruction[0:1]
		amount, err := strconv.Atoi(instruction[1:])
		if err != nil {
			return err
		}
		switch action {
		case "N":
			ship.Move(North, amount)
		case "S":
			ship.Move(South, amount)
		case "E":
			ship.Move(East, amount)
		case "W":
			ship.Move(West, amount)
		case "L":
			ship.Rotate(Rotation{Left, amount})
		case "R":
			ship.Rotate(Rotation{Right, amount})
		case "F":
			ship.Forward(amount)
		}
	}

	return nil
}
