package part1

import (
	"fmt"
	"testing"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"F10",
		"N3",
		"F7",
		"R90",
		"F11",
	}
	expected := 25
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestNewShip(t *testing.T) {
	ship := NewShip()
	if ship.CardinalDirection != East {
		t.Fatalf("direction expected: %s, actual: %s", East, ship.CardinalDirection)
	}
	if ship.X != 0 {
		t.Fatalf("X expected: %d, actual: %d", 0, ship.X)
	}
	if ship.Y != 0 {
		t.Fatalf("Y expected: %d, actual: %d", 0, ship.Y)
	}
}

func TestShip_Rotate(t *testing.T) {
	var tests = []struct {
		startingDir CardinalDirection
		rotation    Rotation
		expectedDir CardinalDirection
	}{
		{North, Rotation{Right, 90}, East},
		{East, Rotation{Right, 90}, South},
		{South, Rotation{Right, 90}, West},
		{West, Rotation{Right, 90}, North},

		{North, Rotation{Right, 180}, South},
		{East, Rotation{Right, 180}, West},
		{South, Rotation{Right, 180}, North},
		{West, Rotation{Right, 180}, East},

		{North, Rotation{Right, 270}, West},
		{East, Rotation{Right, 270}, North},
		{South, Rotation{Right, 270}, East},
		{West, Rotation{Right, 270}, South},

		{North, Rotation{Right, 360}, North},
		{East, Rotation{Right, 360}, East},
		{South, Rotation{Right, 360}, South},
		{West, Rotation{Right, 360}, West},

		{North, Rotation{Left, 90}, West},
		{East, Rotation{Left, 90}, North},
		{South, Rotation{Left, 90}, East},
		{West, Rotation{Left, 90}, South},

		{North, Rotation{Left, 180}, South},
		{East, Rotation{Left, 180}, West},
		{South, Rotation{Left, 180}, North},
		{West, Rotation{Left, 180}, East},

		{North, Rotation{Left, 270}, East},
		{East, Rotation{Left, 270}, South},
		{South, Rotation{Left, 270}, West},
		{West, Rotation{Left, 270}, North},

		{North, Rotation{Left, 360}, North},
		{East, Rotation{Left, 360}, East},
		{South, Rotation{Left, 360}, South},
		{West, Rotation{Left, 360}, West},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s-%s", test.startingDir, test.rotation), func(t *testing.T) {
			ship := NewShip()
			ship.CardinalDirection = test.startingDir
			ship.Rotate(test.rotation)
			expected := test.expectedDir
			actual := ship.CardinalDirection

			if actual != expected {
				t.Fatalf("direction expected: %s, actual: %s", expected, actual)
			}
		})
	}
}

func TestShip_Move(t *testing.T) {
	movementValue := 10
	var tests = []struct {
		direction CardinalDirection
		expectedX int
		expectedY int
	}{
		{North, 0, movementValue},
		{East, movementValue, 0},
		{South, 0, -movementValue},
		{West, -movementValue, 0},
	}

	for _, test := range tests {
		t.Run(test.direction.String(), func(t *testing.T) {
			ship := NewShip()
			ship.Move(test.direction, movementValue)

			if ship.X != test.expectedX {
				t.Fatalf("X expected: %d, actual: %d", test.expectedX, ship.X)
			}
			if ship.Y != test.expectedY {
				t.Fatalf("Y expected: %d, actual: %d", test.expectedY, ship.Y)
			}
		})
	}
}

func TestShip_Forward(t *testing.T) {
	movementValue := 10
	var tests = []struct {
		direction CardinalDirection
		expectedX int
		expectedY int
	}{
		{North, 0, movementValue},
		{East, movementValue, 0},
		{South, 0, -movementValue},
		{West, -movementValue, 0},
	}

	for _, test := range tests {
		t.Run(test.direction.String(), func(t *testing.T) {
			ship := NewShip()
			ship.CardinalDirection = test.direction
			ship.Forward(movementValue)

			if ship.X != test.expectedX {
				t.Fatalf("X expected: %d, actual: %d", test.expectedX, ship.X)
			}
			if ship.Y != test.expectedY {
				t.Fatalf("Y expected: %d, actual: %d", test.expectedY, ship.Y)
			}
		})
	}
}

func TestShip_Travel(t *testing.T) {
	var instructions = []string{
		"F10",
		"N3",
		"F7",
		"R90",
		"F11",
	}
	ship := NewShip()
	err := ship.Travel(instructions)
	expectedX := 17
	expectedY := -8
	if err != nil {
		t.Error(err)
	}
	if ship.X != expectedX {
		t.Fatalf("X expected: %d, actual: %d", expectedX, ship.X)
	}
	if ship.Y != expectedY {
		t.Fatalf("Y expected: %d, actual: %d", expectedY, ship.Y)
	}
}
