package part1

import (
	"fmt"
	"strings"
	"testing"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"class: 1-3 or 5-7",
		"row: 6-11 or 33-44",
		"seat: 13-40 or 45-50",
		"",
		"your Ticket:",
		"7,1,14",
		"",
		"nearby tickets:",
		"7,3,47",
		"40,4,50",
		"55,2,20",
		"38,6,12",
	}
	expected := 71
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestParseSections(t *testing.T) {
	var inputs = []string{
		"class: 1-3 or 5-7",
		"row: 6-11 or 33-44",
		"seat: 13-40 or 45-50",
		"departure location: 32-615 or 626-955",
		"",
		"your Ticket:",
		"7,1,14",
		"",
		"nearby tickets:",
		"7,3,47",
		"40,4,50",
		"55,2,20",
		"38,6,12",
	}
	expectedMap := map[string][]string{
		"rules:": {
			"class: 1-3 or 5-7",
			"row: 6-11 or 33-44",
			"seat: 13-40 or 45-50",
			"departure location: 32-615 or 626-955",
		},
		"your Ticket:": {
			"7,1,14",
		},
		"nearby tickets:": {
			"7,3,47",
			"40,4,50",
			"55,2,20",
			"38,6,12",
		},
	}
	actualMap := ParseSections(inputs)
	for k, v := range expectedMap {
		t.Run(k, func(t *testing.T) {
			expected := strings.Join(v, "\n")
			if _, ok := actualMap[k]; !ok {
				t.Fatalf("expected key '%s' not found in actual", k)
			}
			actual := strings.Join(actualMap[k], "\n")
			if actual != expected {
				t.Fatalf("expected: %s, actual: %s", expected, actual)
			}
		})
	}
}

func TestGetInvalid(t *testing.T) {
	rules, err := NewRuleSet(
		"class: 1-3 or 5-7",
		"row: 6-11 or 33-44",
		"seat: 13-40 or 45-50",
	)
	if err != nil {
		t.Fatal(err)
	}
	var tests = []struct {
		ticket   Ticket
		expected int
	}{
		{[]int{7, 3, 47}, -1},
		{[]int{40, 4, 50}, 4},
		{[]int{55, 2, 20}, 55},
		{[]int{38, 6, 12}, 12},
	}
	for _, test := range tests {
		test := test
		t.Run(fmt.Sprintf("%v", test.ticket), func(t *testing.T) {
			t.Parallel()
			expected := test.expected
			actual := rules.GetInvalid(test.ticket)
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}
