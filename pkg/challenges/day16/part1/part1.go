package part1

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/utilities"
	"strconv"
	"strings"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	sections := ParseSections(input)
	ruleSet, err := NewRuleSet(sections["rules:"]...)
	if err != nil {
		return -1, err
	}
	var invalids []int
	for _, line := range sections["nearby tickets:"] {
		ticket, err := utilities.AsToIs(strings.Split(line, ","))
		if err != nil {
			return -1, err
		}
		if invalid := ruleSet.GetInvalid(ticket); invalid >= 0 {
			invalids = append(invalids, invalid)
		}
	}
	answer = utilities.Sum(invalids...)

	return
}

func ParseSections(input []string) map[string][]string {
	if input[len(input)-1] != "" {
		input = append(input, "")
	}
	sections := map[string][]string{}
	var lines []string
	header := "rules:"
	isHeader := false
	for _, line := range input {
		if line == "" {
			sections[header] = lines[:]
			lines = []string{}
			isHeader = true
		} else if isHeader {
			header = line
			isHeader = false
		} else {
			lines = append(lines, line)
		}
	}
	return sections
}

type (
	Ticket     []int
	Constraint struct {
		min, max int
	}
	Rule struct {
		name        string
		constraints []*Constraint
	}
	RuleSet struct {
		rules []*Rule
	}
)

func NewRuleSet(lines ...string) (*RuleSet, error) {
	ruleSet := &RuleSet{
		rules: []*Rule{},
	}
	for _, rule := range lines {
		parts := strings.Split(rule, ": ")
		constraints := strings.Split(parts[1], " or ")
		r, err := NewRule(parts[0], constraints...)
		if err != nil {
			return nil, err
		}
		ruleSet.rules = append(ruleSet.rules, r)
	}
	return ruleSet, nil
}

func NewRule(name string, constraints ...string) (*Rule, error) {
	rule := &Rule{
		name:        name,
		constraints: []*Constraint{},
	}
	for _, constraint := range constraints {
		c, err := NewConstraint(constraint)
		if err != nil {
			return nil, err
		}
		rule.constraints = append(rule.constraints, c)
	}
	return rule, nil
}

func NewConstraint(range_ string) (*Constraint, error) {
	parts := strings.Split(range_, "-")
	min, err := strconv.Atoi(parts[0])
	if err != nil {
		return nil, err
	}
	max, err := strconv.Atoi(parts[1])
	if err != nil {
		return nil, err
	}
	return &Constraint{
		min: min,
		max: max,
	}, nil
}

func (ruleSet RuleSet) GetInvalid(ticket Ticket) int {
	for _, num := range ticket {
		valid := false
		for _, rule := range ruleSet.rules {
			if rule.Valid(num) {
				valid = true
				break
			}
		}
		if !valid {
			return num
		}
	}

	return -1
}

func (rule Rule) Valid(num int) bool {
	for _, constraint := range rule.constraints {
		if constraint.min <= num && num <= constraint.max {
			return true
		}
	}
	return false
}
