package part2

import (
	"fmt"
	"strings"
	"testing"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{
		"class: 0-1 or 4-19",
		"departure row: 0-5 or 8-19",
		"departure seat: 0-13 or 16-19",
		"",
		"your ticket:",
		"11,12,13",
		"",
		"nearby tickets:",
		"3,9,18",
		"15,1,5",
		"5,14,9",
	}
	expected := 143
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestParseSections(t *testing.T) {
	var inputs = []string{
		"class: 1-3 or 5-7",
		"row: 6-11 or 33-44",
		"seat: 13-40 or 45-50",
		"departure location: 32-615 or 626-955",
		"",
		"your Ticket:",
		"7,1,14",
		"",
		"nearby tickets:",
		"7,3,47",
		"40,4,50",
		"55,2,20",
		"38,6,12",
	}
	expectedMap := map[string][]string{
		"rules:": {
			"class: 1-3 or 5-7",
			"row: 6-11 or 33-44",
			"seat: 13-40 or 45-50",
			"departure location: 32-615 or 626-955",
		},
		"your Ticket:": {
			"7,1,14",
		},
		"nearby tickets:": {
			"7,3,47",
			"40,4,50",
			"55,2,20",
			"38,6,12",
		},
	}
	actualMap := ParseSections(inputs)
	for k, v := range expectedMap {
		t.Run(k, func(t *testing.T) {
			expected := strings.Join(v, "\n")
			if _, ok := actualMap[k]; !ok {
				t.Fatalf("expected key '%s' not found in actual", k)
			}
			actual := strings.Join(actualMap[k], "\n")
			if actual != expected {
				t.Fatalf("expected: %s, actual: %s", expected, actual)
			}
		})
	}
}

func TestGetInvalid(t *testing.T) {
	rules, err := NewRuleSet(
		"class: 1-3 or 5-7",
		"row: 6-11 or 33-44",
		"seat: 13-40 or 45-50",
	)
	if err != nil {
		t.Fatal(err)
	}
	var tests = []struct {
		ticket   Ticket
		expected int
	}{
		{[]int{7, 3, 47}, -1},
		{[]int{40, 4, 50}, 4},
		{[]int{55, 2, 20}, 55},
		{[]int{38, 6, 12}, 12},
	}
	for _, test := range tests {
		test := test
		t.Run(fmt.Sprintf("%v", test.ticket), func(t *testing.T) {
			t.Parallel()
			expected := test.expected
			actual := rules.GetInvalid(test.ticket)
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}

func TestRules_Identify(t *testing.T) {
	rules, err := NewRuleSet(
		"class: 0-1 or 4-19",
		"row: 0-5 or 8-19",
		"seat: 0-13 or 16-19",
	)
	if err != nil {
		t.Fatal(err)
	}
	myTicket := Ticket{11, 12, 13}
	validTickets := []Ticket{
		{3, 9, 18},
		{15, 1, 5},
		{5, 14, 9},
	}
	expected := map[string]int{
		"class": 12,
		"row":   11,
		"seat":  13,
	}
	actual := rules.Identify(myTicket, validTickets)
	if len(actual) != len(expected) {
		t.Fatalf("len expected: %d, len actual: %d", len(expected), len(actual))
	}
	for key, exp := range expected {
		if act, ok := actual[key]; !ok {
			t.Fatalf("expected key '%s' was not found in actual", key)
		} else if act != exp {
			t.Fatalf("%s expected: %d, %s actual: %d", key, exp, key, act)
		}
	}
}
