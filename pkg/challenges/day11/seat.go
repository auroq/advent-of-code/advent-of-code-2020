package day11

const (
	occupied = "#"
	empty    = "L"
)

type Seat string

func NewSeat(val rune) (Seat, bool) {
	seat, ok := map[rune]Seat{
		'#': occupied,
		'L': empty,
	}[val]
	return seat, ok
}

func (seat Seat) String() string {
	if val, ok := map[Seat]string{
		occupied: "#",
		empty:    "L",
	}[seat]; ok {
		return val
	}
	return "."
}

func (seat Seat) Occupied() bool {
	return seat == occupied
}

func (seat Seat) Invert() Seat {
	if seat.Occupied() {
		return empty
	}

	return occupied
}
