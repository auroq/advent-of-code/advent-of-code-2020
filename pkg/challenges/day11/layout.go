package day11

type Layout struct {
	Height int
	Width  int
	Seats  map[int]map[int]Seat
}

func EmptyLayout(height, width int) Layout {
	return Layout{
		Height: height,
		Width:  width,
		Seats:  map[int]map[int]Seat{},
	}
}

func NewLayout(input []string) Layout {
	layout := Layout{
		Height: len(input),
		Width:  len(input[0]),
		Seats:  map[int]map[int]Seat{},
	}
	for rowIndex, row := range input {
		layout.Seats[rowIndex] = map[int]Seat{}
		for colIndex, col := range row {
			if seat, ok := NewSeat(col); ok {
				layout.Seats[rowIndex][colIndex] = seat
			}
		}
	}

	return layout
}

func (layout Layout) Copy() Layout {
	copyLayout := Layout{}
	for rowIndex, row := range layout.Seats {
		layout.Seats[rowIndex] = map[int]Seat{}
		for colIndex, col := range row {
			layout.Seats[rowIndex][colIndex] = col
		}
	}

	return copyLayout
}

func (layout Layout) String() (str string) {
	for row := 0; row < layout.Height; row++ {
		for col := 0; col < layout.Width; col++ {
			if cell, ok := layout.Seats[row][col]; ok {
				str += cell.String()
			} else {
				str += "."
			}
		}
		str += "\n"
	}
	return
}

func (layout Layout) Equals(other Layout) bool {
	for rowIndex := range layout.Seats {
		for colIndex := range layout.Seats[rowIndex] {
			if layout.Seats[rowIndex][colIndex] != other.Seats[rowIndex][colIndex] {
				return false
			}
		}
	}

	return true
}

func (layout Layout) CountOccupied() (count int) {
	for _, row := range layout.Seats {
		for _, col := range row {
			if col == occupied {
				count++
			}
		}
	}

	return
}
