package part2

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/challenges/day11"
	"strconv"
	"strings"
	"testing"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{
		"L.LL.LL.LL",
		"LLLLLLL.LL",
		"L.L.L..L..",
		"LLLL.LL.LL",
		"L.LL.LL.LL",
		"L.LLLLL.LL",
		"..L.L.....",
		"LLLLLLLLLL",
		"L.LLLLLL.L",
		"L.LLLLL.LL",
	}
	expected := 26
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestGetOccupied(t *testing.T) {
	var tests = []struct {
		lines    []string
		row, col int
		expected int
	}{
		{
			[]string{
				".......#.",
				"...#.....",
				".#.......",
				".........",
				"..#L....#",
				"....#....",
				".........",
				"#........",
				"...#.....",
			}, 4, 3, 8,
		},
		{
			[]string{
				".............",
				".L.L.#.#.#.#.",
				".............",
			}, 1, 1, 0,
		},
		{
			[]string{
				".##.##.",
				"#.#.#.#",
				"##...##",
				"...L...",
				"##...##",
				"#.#.#.#",
				".##.##.",
			},
			3, 3, 0,
		},
		{
			[]string{
				"#.##.##.##",
				"#######.##",
			}, 0, 0, 3,
		},
	}
	for i, test := range tests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			layout := day11.NewLayout(test.lines)
			actual := GetOccupied(layout, test.row, test.col)
			expected := test.expected
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}

func TestIterateLayout(t *testing.T) {
	iterations := [][]string{
		{
			"L.LL.LL.LL",
			"LLLLLLL.LL",
			"L.L.L..L..",
			"LLLL.LL.LL",
			"L.LL.LL.LL",
			"L.LLLLL.LL",
			"..L.L.....",
			"LLLLLLLLLL",
			"L.LLLLLL.L",
			"L.LLLLL.LL",
		}, {
			"#.##.##.##",
			"#######.##",
			"#.#.#..#..",
			"####.##.##",
			"#.##.##.##",
			"#.#####.##",
			"..#.#.....",
			"##########",
			"#.######.#",
			"#.#####.##",
		}, {
			"#.LL.LL.L#",
			"#LLLLLL.LL",
			"L.L.L..L..",
			"LLLL.LL.LL",
			"L.LL.LL.LL",
			"L.LLLLL.LL",
			"..L.L.....",
			"LLLLLLLLL#",
			"#.LLLLLL.L",
			"#.LLLLL.L#",
		}, {
			"#.L#.##.L#",
			"#L#####.LL",
			"L.#.#..#..",
			"##L#.##.##",
			"#.##.#L.##",
			"#.#####.#L",
			"..#.#.....",
			"LLL####LL#",
			"#.L#####.L",
			"#.L####.L#",
		}, {
			"#.L#.L#.L#",
			"#LLLLLL.LL",
			"L.L.L..#..",
			"##LL.LL.L#",
			"L.LL.LL.L#",
			"#.LLLLL.LL",
			"..L.L.....",
			"LLLLLLLLL#",
			"#.LLLLL#.L",
			"#.L#LL#.L#",
		}, {
			"#.L#.L#.L#",
			"#LLLLLL.LL",
			"L.L.L..#..",
			"##L#.#L.L#",
			"L.L#.#L.L#",
			"#.L####.LL",
			"..#.#.....",
			"LLL###LLL#",
			"#.LLLLL#.L",
			"#.L#LL#.L#",
		}, {
			"#.L#.L#.L#",
			"#LLLLLL.LL",
			"L.L.L..#..",
			"##L#.#L.L#",
			"L.L#.LL.L#",
			"#.LLLL#.LL",
			"..#.L.....",
			"LLL###LLL#",
			"#.LLLLL#.L",
			"#.L#LL#.L#",
		},
	}
	type iterateTest struct {
		layout   day11.Layout
		expected string
	}
	var tests []iterateTest
	for i := 0; i < len(iterations)-1; i++ {
		tests = append(tests, iterateTest{
			day11.NewLayout(iterations[i]),
			strings.Join(iterations[i+1], ""),
		})
	}
	for i, test := range tests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			expected := test.expected
			actual := strings.ReplaceAll(iterateLayout(test.layout).String(), "\n", "")
			if actual != expected {
				t.Fatalf("expected:\n%s\n\nactual:\n%s", expected, actual)
			}
		})
	}
}
