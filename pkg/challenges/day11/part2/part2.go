package part2

import (
	"fmt"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/challenges/day11"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	layout := day11.NewLayout(input)
	nextLayout := iterateLayout(layout)
	for !nextLayout.Equals(layout) {
		layout = nextLayout
		nextLayout = iterateLayout(layout)
	}
	answer = layout.CountOccupied()

	return
}

func iterateSeat(seat day11.Seat, layout day11.Layout, row, col int) day11.Seat {
	count := GetOccupied(layout, row, col)
	if !seat.Occupied() && count == 0 {
		return seat.Invert()
	}
	if seat.Occupied() && count >= 5 {
		return seat.Invert()
	}
	return seat
}

func GetOccupied(layout day11.Layout, row, col int) (count int) {
	count = 0
	type movement func(x, y int) (int, int, error)
	validate := func(x, y int) (int, int, error) {
		if x < 0 || y < 0 || x >= layout.Width || y >= layout.Height {
			return -1, -1, fmt.Errorf("%d,%d is not a valid seat", x, y)
		}
		return x, y, nil
	}
	directions := []movement{
		func(x, y int) (int, int, error) { return validate(x-1, y-1) },
		func(x, y int) (int, int, error) { return validate(x, y-1) },
		func(x, y int) (int, int, error) { return validate(x+1, y-1) },
		func(x, y int) (int, int, error) { return validate(x-1, y) },
		func(x, y int) (int, int, error) { return validate(x+1, y) },
		func(x, y int) (int, int, error) { return validate(x-1, y+1) },
		func(x, y int) (int, int, error) { return validate(x, y+1) },
		func(x, y int) (int, int, error) { return validate(x+1, y+1) },
	}
	for _, direction := range directions {
		x, y, err := direction(col, row)
		if err != nil {
			continue
		}
		foundSeat := false
		for !foundSeat {
			if seat, ok := layout.Seats[y][x]; ok {
				foundSeat = true
				if seat.Occupied() {
					count++
				}
			} else {
				x, y, err = direction(x, y)
				if err != nil {
					break
				}
			}
		}
	}

	return
}

func iterateLayout(layout day11.Layout) day11.Layout {
	nextLayout := day11.EmptyLayout(layout.Height, layout.Width)
	for rowIndex, row := range layout.Seats {
		nextLayout.Seats[rowIndex] = map[int]day11.Seat{}
		for colIndex, col := range row {
			nextLayout.Seats[rowIndex][colIndex] = iterateSeat(col, layout, rowIndex, colIndex)
		}
	}

	return nextLayout
}
