package part1

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/challenges/day11"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/utilities"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	layout := day11.NewLayout(input)
	nextLayout := iterateLayout(layout)
	for !nextLayout.Equals(layout) {
		layout = nextLayout
		nextLayout = iterateLayout(layout)
	}
	answer = layout.CountOccupied()

	return
}

func iterateSeat(seat day11.Seat, layout day11.Layout, row, col int) day11.Seat {
	count := GetOccupied(layout, row, col)
	if !seat.Occupied() && count == 0 {
		return seat.Invert()
	}
	if seat.Occupied() && count >= 4 {
		return seat.Invert()
	}
	return seat
}

func GetAdjacent(layout day11.Layout, row, col int) (seats []day11.Seat) {
	for r := utilities.Max(row-1, 0); r < utilities.Min(row+1, layout.Height-1); r++ {
		for c := utilities.Max(col-1, 0); c < utilities.Min(col+1, layout.Width-1); c++ {
			if r == row && c == col {
				continue
			}
			if seat, ok := layout.Seats[r][c]; ok {
				seats = append(seats, seat)
			}
		}
	}

	return
}

func GetOccupied(layout day11.Layout, row, col int) (count int) {
	count = 0
	for r := utilities.Max(row-1, 0); r <= utilities.Min(row+1, layout.Height-1); r++ {
		for c := utilities.Max(col-1, 0); c <= utilities.Min(col+1, layout.Width-1); c++ {
			if r == row && c == col {
				continue
			}
			if seat, ok := layout.Seats[r][c]; ok {
				if seat.Occupied() {
					count++
				}
			}
		}
	}

	return
}

func iterateLayout(layout day11.Layout) day11.Layout {
	nextLayout := day11.EmptyLayout(layout.Height, layout.Width)
	for rowIndex, row := range layout.Seats {
		nextLayout.Seats[rowIndex] = map[int]day11.Seat{}
		for colIndex, col := range row {
			nextLayout.Seats[rowIndex][colIndex] = iterateSeat(col, layout, rowIndex, colIndex)
		}
	}

	return nextLayout
}
