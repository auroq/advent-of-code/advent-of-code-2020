package part1

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/challenges/day11"
	"strconv"
	"strings"
	"testing"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"L.LL.LL.LL",
		"LLLLLLL.LL",
		"L.L.L..L..",
		"LLLL.LL.LL",
		"L.LL.LL.LL",
		"L.LLLLL.LL",
		"..L.L.....",
		"LLLLLLLLLL",
		"L.LLLLLL.L",
		"L.LLLLL.LL",
	}
	expected := 37
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestGetOccupied(t *testing.T) {
	lines := []string{
		"#.##.##.##",
		"#######.##",
	}
	layout := day11.NewLayout(lines)
	actual := GetOccupied(layout, 0, 2)
	expected := 4
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestIterate(t *testing.T) {
	iterations := [][]string{
		{
			"#.##.##.##",
			"#######.##",
			"#.#.#..#..",
			"####.##.##",
			"#.##.##.##",
			"#.#####.##",
			"..#.#.....",
			"##########",
			"#.######.#",
			"#.#####.##",
		}, {
			"#.LL.L#.##",
			"#LLLLLL.L#",
			"L.L.L..L..",
			"#LLL.LL.L#",
			"#.LL.LL.LL",
			"#.LLLL#.##",
			"..L.L.....",
			"#LLLLLLLL#",
			"#.LLLLLL.L",
			"#.#LLLL.##",
		}, {
			"#.##.L#.##",
			"#L###LL.L#",
			"L.#.#..#..",
			"#L##.##.L#",
			"#.##.LL.LL",
			"#.###L#.##",
			"..#.#.....",
			"#L######L#",
			"#.LL###L.L",
			"#.#L###.##",
		}, {
			"#.#L.L#.##",
			"#LLL#LL.L#",
			"L.L.L..#..",
			"#LLL.##.L#",
			"#.LL.LL.LL",
			"#.LL#L#.##",
			"..L.L.....",
			"#L#LLLL#L#",
			"#.LLLLLL.L",
			"#.#L#L#.##",
		}, {
			"#.#L.L#.##",
			"#LLL#LL.L#",
			"L.#.L..#..",
			"#L##.##.L#",
			"#.#L.LL.LL",
			"#.#L#L#.##",
			"..L.L.....",
			"#L#L##L#L#",
			"#.LLLLLL.L",
			"#.#L#L#.##",
		},
	}
	type iterateTest struct {
		layout   day11.Layout
		expected string
	}
	var tests []iterateTest
	for i := 0; i < len(iterations)-1; i++ {
		tests = append(tests, iterateTest{
			day11.NewLayout(iterations[i]),
			strings.Join(iterations[i+1], ""),
		})
	}
	for i, test := range tests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			expected := test.expected
			actual := strings.ReplaceAll(iterateLayout(test.layout).String(), "\n", "")
			if actual != expected {
				t.Fatalf("expected:\n%s\n\nactual:\n%s", expected, actual)
			}
		})
	}
}
