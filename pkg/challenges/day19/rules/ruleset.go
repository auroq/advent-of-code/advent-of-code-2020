package rules

import (
	"fmt"
	"sort"
	"strings"
)

type RuleSet struct {
	rules map[string]*Rule
}

func NewRuleSet(lines []string) *RuleSet {
	ruleSet := &RuleSet{
		rules: map[string]*Rule{},
	}

	for _, line := range lines {
		parts := strings.Split(line, ":")
		id := parts[0]
		rule := ruleSet.GetOrAddRule(id)
		if ruleValue := strings.TrimSpace(parts[1]); strings.Contains(ruleValue, "\"") {
			rule.value = strings.Split(parts[1], "\"")[1]
		} else {
			ruleValueParts := strings.Split(ruleValue, "|")
			for _, ruleValuePart := range ruleValueParts {
				ruleGroup := RuleGroup{}
				for _, ruleId := range strings.Split(strings.TrimSpace(ruleValuePart), " ") {
					ruleGroup = append(ruleGroup, ruleSet.GetOrAddRule(ruleId))
				}
				rule.ruleGroups = append(rule.ruleGroups, ruleGroup)
			}
		}
	}

	return ruleSet
}

func (ruleSet RuleSet) GetOrAddRule(id string) *Rule {
	if _, ok := ruleSet.rules[id]; !ok {
		ruleSet.rules[id] = &Rule{id: id}
	}

	return ruleSet.rules[id]
}

func (ruleSet RuleSet) Equals(other RuleSet) bool {
	for key, rule := range ruleSet.rules {
		if otherRule, ok := other.rules[key]; !ok {
			return false
		} else {
			if !(*rule).Equals(*otherRule) {
				return false
			}
		}
	}

	return true
}

func (ruleSet RuleSet) String() string {
	var rulesStrings []string
	for id, rule := range ruleSet.rules {
		rulesStrings = append(rulesStrings, fmt.Sprintf("%s: %s", id, rule.String()))
	}
	sort.Strings(rulesStrings)

	return strings.Join(rulesStrings, "\n")
}

func (ruleSet RuleSet) Matches(id string, message string) bool {
	if rule, ok := ruleSet.rules[id]; !ok {
		return false
	} else {
		leftover, isMatch := rule.Matches(message)
		if leftover != "" {
			return false
		}
		return isMatch
	}
}
