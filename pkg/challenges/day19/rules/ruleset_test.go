package rules

import (
	"testing"
)

func TestNewRuleSet(t *testing.T) {
	input := []string{
		"0: 4 1 5",
		"1: 2 3 | 3 2",
		"2: 4 4 | 5 5",
		"3: 4 5 | 5 4",
		"4: \"a\"",
		"5: \"b\"",
	}
	rule5 := &Rule{id: "5", value: "b"}
	rule4 := &Rule{id: "4", value: "a"}
	rule3 := &Rule{id: "3", ruleGroups: []RuleGroup{{rule4, rule5}, {rule5, rule4}}}
	rule2 := &Rule{id: "2", ruleGroups: []RuleGroup{{rule4, rule4}, {rule5, rule5}}}
	rule1 := &Rule{id: "1", ruleGroups: []RuleGroup{{rule2, rule3}, {rule3, rule2}}}
	rule0 := &Rule{id: "0", ruleGroups: []RuleGroup{{rule4, rule1, rule5}}}
	expected := RuleSet{
		rules: map[string]*Rule{
			"0": rule0,
			"1": rule1,
			"2": rule2,
			"3": rule3,
			"4": rule4,
			"5": rule5,
		},
	}
	actual := NewRuleSet(input)
	if !actual.Equals(expected) {
		t.Fatalf("expected:\n%s\n\nactual:\n%s", expected, actual)
	}
}

func TestRuleSet_Matches(t *testing.T) {
	ruleSet := NewRuleSet([]string{
		"0: 4 1 5",
		"1: 2 3 | 3 2",
		"2: 4 4 | 5 5",
		"3: 4 5 | 5 4",
		"4: \"a\"",
		"5: \"b\"",
	})
	var tests = []struct {
		message  string
		expected bool
	}{
		{"ababbb", true},
		{"bababa", false},
		{"abbbab", true},
		{"aaabbb", false},
		{"aaaabbb", false},
	}
	for _, test := range tests {
		test := test
		t.Run(test.message, func(t *testing.T) {
			expected := test.expected
			actual := ruleSet.Matches("0", test.message)
			if actual != expected {
				t.Fatalf("expected: %t, actual: %t", expected, actual)
			}
		})
	}
}

func TestRuleSet_Matches_Long(t *testing.T) {
	ruleSet := NewRuleSet([]string{
		"42: 9 14 | 10 1",
		"9: 14 27 | 1 26",
		"10: 23 14 | 28 1",
		"1: \"a\"",
		"11: 42 31 | 42 11 31", // replaced
		"5: 1 14 | 15 1",
		"19: 14 1 | 14 14",
		"12: 24 14 | 19 1",
		"16: 15 1 | 14 14",
		"31: 14 17 | 1 13",
		"6: 14 14 | 1 14",
		"2: 1 24 | 14 4",
		"0: 8 11",
		"13: 14 3 | 1 12",
		"15: 1 | 14",
		"17: 14 2 | 1 7",
		"23: 25 1 | 22 14",
		"28: 16 1",
		"4: 1 1",
		"20: 14 14 | 1 15",
		"3: 5 14 | 16 1",
		"27: 1 6 | 14 18",
		"14: \"b\"",
		"21: 14 1 | 1 14",
		"25: 1 1 | 1 14",
		"22: 14 14",
		"8: 42 | 42 8", // Replaced
		"26: 14 22 | 1 20",
		"18: 15 15",
		"7: 14 5 | 1 21",
		"24: 14 1",
	})
	var tests = []struct {
		message  string
		expected bool
	}{
		{"bbabbbbaabaabba", true},
		{"ababaaaaaabaaab", true},
		{"ababaaaaabbbaba", true},
		{"aaaabbaaaabbaaa", false},
		{"aaaaabbaabaaaaababaa", true},
		{"baabbaaaabbaaaababbaababb", true},
		{"bbbbbbbaaaabbbbaaabbabaaa", true},
		{"abbbbabbbbaaaababbbbbbaaaababb", true},
		{"babbbbaabbbbbabbbbbbaabaaabaaa", true},
		{"babaaabbbaaabaababbaabababaaab", false},
		{"bbbababbbbaaaaaaaabbababaaababaabab", true},
		{"aaaabbaabbaaaaaaabbbabbbaaabbaabaaa", true},
		{"aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba", true},
		{"abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa", false},
		{"aaabbbbbbaaaabaababaabababbabaaabbababababaaa", true},
	}
	for _, test := range tests {
		test := test
		t.Run(test.message, func(t *testing.T) {
			expected := test.expected
			actual := ruleSet.Matches("0", test.message)
			if actual != expected {
				t.Fatalf("expected: %t, actual: %t", expected, actual)
			}
		})
	}
}
