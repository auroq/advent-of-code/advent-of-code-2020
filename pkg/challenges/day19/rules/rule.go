package rules

import (
	"fmt"
	"strings"
)

type (
	Rule struct {
		ruleGroups []RuleGroup
		value      string
		id         string
	}
	RuleGroup []*Rule
)

func (rule Rule) Equals(other Rule) bool {
	if rule.value != other.value {
		return false
	}
	if len(rule.ruleGroups) != len(other.ruleGroups) {
		return false
	}
	for i := range rule.ruleGroups {
		if !rule.ruleGroups[i].Equals(other.ruleGroups[i]) {
			return false
		}
	}

	return true
}

func (ruleGroup RuleGroup) Equals(other RuleGroup) bool {
	if len(ruleGroup) != len(other) {
		return false
	}

	for i := range ruleGroup {
		if !(*ruleGroup[i]).Equals(*other[i]) {
			return false
		}
	}

	return true
}

func (rule Rule) String() string {
	if rule.value != "" {
		return fmt.Sprintf("\"%s\"", rule.value)
	} else {
		var idGroups []string
		for _, group := range rule.ruleGroups {
			var ids []string
			for _, parentRule := range group {
				ids = append(ids, parentRule.id)
			}
			idGroups = append(idGroups, strings.Join(ids, " "))
		}
		return strings.Join(idGroups, " | ")
	}
}

func (rule Rule) Matches(value string) (string, bool) {
	if rule.value != "" {
		return value[1:], value[0:1] == rule.value
	}
	for _, ruleGroup := range rule.ruleGroups {
		if leftover, isMatch := ruleGroup.Matches(value); isMatch {
			return leftover, isMatch
		}
	}

	return "", false
}

func (ruleGroup RuleGroup) Matches(value string) (string, bool) {
	if len(ruleGroup) > len(value) {
		return "", false
	}
	val := value
	for _, rule := range ruleGroup {
		if leftover, isMatch := rule.Matches(val); isMatch {
			val = leftover
		} else {
			return "", false
		}
	}
	return val, true
}

//
//func (rule Rule) Transcribe() (values []string) {
//	if rule.value != "" {
//		return []string{rule.value}
//	}
//	for _, group := range rule.ruleGroups {
//		var val string
//		for _, subRule := range group {
//			val =
//		}
//	}
//}
