package part2

import (
	"fmt"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/challenges/day19/rules"
	"strings"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	var ruleLines []string
	var i int
	var line string
	for i, line = range input {
		if line == "" {
			break
		}
		if strings.HasPrefix(line, "8: ") {
			line = "8: 42 | 42 8"
		}
		if strings.HasPrefix(line, "11: ") {
			line = "11: 42 31 | 42 11 31"
		}
		ruleLines = append(ruleLines, line)
	}
	ruleSet := rules.NewRuleSet(ruleLines)
	for _, message := range input[i+1:] {
		if ruleSet.Matches("0", message) {
			answer++
			fmt.Println(message)
		}
	}

	return
}
