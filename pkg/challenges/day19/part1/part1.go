package part1

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/challenges/day19/rules"
	"strings"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	lines := strings.Join(input, "\n")
	parts := strings.Split(lines, "\n\n")
	ruleLines := strings.Split(parts[0], "\n")
	messageLines := strings.Split(parts[1], "\n")

	ruleSet := rules.NewRuleSet(ruleLines)
	for _, message := range messageLines {
		if ruleSet.Matches("0", message) {
			answer++
		}
	}

	return
}
