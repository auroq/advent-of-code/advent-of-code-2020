package part2

import (
	"fmt"
	"testing"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{"0,3,6"}
	expected := 175594
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestGetNth(t *testing.T) {
	var tests = []struct {
		nums     []int
		n        int
		expected int
	}{
		{[]int{0, 3, 6}, 1, 0},
		{[]int{0, 3, 6}, 2, 3},
		{[]int{0, 3, 6}, 3, 6},
		{[]int{0, 3, 6}, 4, 0},
		{[]int{0, 3, 6}, 5, 3},
		{[]int{0, 3, 6}, 6, 3},
		{[]int{0, 3, 6}, 7, 1},
		{[]int{0, 3, 6}, 8, 0},
		{[]int{0, 3, 6}, 9, 4},
		{[]int{0, 3, 6}, 10, 0},
		{[]int{0, 3, 6}, 30000000, 175594},
		{[]int{1, 3, 2}, 30000000, 2578},
		{[]int{2, 1, 3}, 30000000, 3544142},
		{[]int{1, 2, 3}, 30000000, 261214},
		{[]int{2, 3, 1}, 30000000, 6895259},
		{[]int{3, 2, 1}, 30000000, 18},
		{[]int{3, 1, 2}, 30000000, 362},
	}
	for _, test := range tests {
		test := test
		t.Run(fmt.Sprintf("%d of %v", test.n, test.nums), func(t *testing.T) {
			t.Parallel()
			expected := test.expected
			actual := GetNth(test.n, test.nums)
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}
