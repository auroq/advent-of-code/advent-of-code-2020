package part1

import (
	"fmt"
	"testing"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{"0,3,6"}
	expected := 436
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestGetNth(t *testing.T) {
	var tests = []struct {
		nums     []int
		n        int
		expected int
	}{
		{[]int{0, 3, 6}, 1, 0},
		{[]int{0, 3, 6}, 2, 3},
		{[]int{0, 3, 6}, 3, 6},
		{[]int{0, 3, 6}, 4, 0},
		{[]int{0, 3, 6}, 5, 3},
		{[]int{0, 3, 6}, 6, 3},
		{[]int{0, 3, 6}, 7, 1},
		{[]int{0, 3, 6}, 8, 0},
		{[]int{0, 3, 6}, 9, 4},
		{[]int{0, 3, 6}, 10, 0},
		{[]int{0, 3, 6}, 2020, 436},
		{[]int{1, 3, 2}, 2020, 1},
		{[]int{2, 1, 3}, 2020, 10},
		{[]int{1, 2, 3}, 2020, 27},
		{[]int{2, 3, 1}, 2020, 78},
		{[]int{3, 2, 1}, 2020, 438},
		{[]int{3, 1, 2}, 2020, 1836},
	}
	for _, test := range tests {
		t.Run(fmt.Sprintf("%d of %v", test.n, test.nums), func(t *testing.T) {
			expected := test.expected
			actual := GetNth(test.n, test.nums)
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}
