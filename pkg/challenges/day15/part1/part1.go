package part1

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/utilities"
	"strings"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	nums, err := utilities.AsToIs(strings.Split(input[0], ","))
	if err != nil {
		return -1, err
	}
	answer = GetNth(2020, nums)
	return
}

func GetNth(n int, nums []int) (val int) {
	n--
	if n < len(nums) {
		return nums[n]
	}
	for val = range getNs(nums, n) {
	}
	return
}

func getNs(nums []int, count int) chan int {
	ns := make(chan int, len(nums))
	spoken := map[int]indexes{}
	for i, num := range nums {
		spoken[num] = addIndex(spoken[num], i)
		ns <- num
	}
	lastVal := nums[len(nums)-1]
	go func() {
		for n := len(nums); n <= count; n++ {
			nextVal := 0
			if lastIndex, found := spoken[lastVal]; found && len(lastIndex) > 1 {
				nextVal = lastIndex.diff()
			} else {
				nextVal = 0
			}
			ns <- nextVal
			lastVal = nextVal
			spoken[nextVal] = addIndex(spoken[nextVal], n)
		}
		close(ns)
	}()
	return ns
}

type indexes []int

func addIndex(indexes indexes, next int) indexes {
	if len(indexes) < 1 {
		return []int{next}
	}
	return append([]int{next}, indexes[0:1]...)
}

func (indexes indexes) diff() int {
	return indexes[0] - indexes[1]
}
