package part2

import (
	"fmt"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/challenges/day20/pocket"
	"strings"
)

type Part2 struct{}

var seaMonster = []string{
	"..................#.",
	"#....##....##....###",
	".#..#..#..#..#..#...",
}

func (part Part2) Run(input []string) (answer int, err error) {
	image := pocket.NewImage(input)
	assembledGrid := image.Assembled()
	fmt.Println(assembledGrid)
	fullCount := assembledGrid.CountOccupied()
	seaMonsters := CountSeaMonsters(assembledGrid)
	seaMonsterSpacesCount := pocket.NewGrid(seaMonster).CountOccupied()
	fmt.Printf("SpacesInGrid: %d, SeaMonsters: %d, SpacesPerMonster: %d\n", fullCount, seaMonsters, seaMonsterSpacesCount)
	answer = fullCount - (seaMonsterSpacesCount * seaMonsters)
	fmt.Printf("%d - (%d * %d) = %d\n", fullCount, seaMonsterSpacesCount, seaMonsters, answer)

	return
}

func CountSeaMonsters(grid pocket.Grid) int {
	transformationLists := [][]pocket.Transformation{
		nil,
		{pocket.Half},
		{pocket.FlipX},
		{pocket.FlipY},
		{pocket.Clockwise},
		{pocket.CounterClockwise},
		{pocket.Clockwise, pocket.FlipX},
		{pocket.CounterClockwise, pocket.FlipX},
	}
	maxCount := 0
	for _, transformationList := range transformationLists {
		count := 0
		gridToScan := grid.Copy()
		for _, transformation := range transformationList {
			gridToScan = gridToScan.Transform(transformation)
		}
		lines := strings.Split(gridToScan.String(), "\n")
		for i := 0; i <= len(lines)-len(seaMonster); i++ {
			if horizontalCount := countSeaMonstersHorizontally(lines[i : i+3]); horizontalCount > 0 {
				count += horizontalCount
				//i += 3 - 1 // 1 is already added by the loop
			}
		}
		fmt.Printf("transformations: %v, count: %d\n", transformationList, count)
		if count > maxCount {
			maxCount = count
		}
	}

	return maxCount
}

func countSeaMonstersHorizontally(lines []string) (count int) {
	if len(lines) != len(seaMonster) {
		return
	}
	xDiff := len(lines[0]) - len(seaMonster[0])
	if xDiff < 0 {
		return
	}
	for i := 0; i <= xDiff; i++ {
		monsterGrid := pocket.NewGrid(padMonster(i))
		linesGrid := pocket.NewGrid(lines)
		contains := true
		for position := range monsterGrid.Cells {
			if _, ok := linesGrid.Cells[position]; !ok {
				contains = false
				break
			}
		}
		if contains {
			count++
			i += len(seaMonster[0]) - 1 // 1 is already added by the loop
		}
	}

	return
}

func padMonster(count int) (paddedMonster []string) {
	for _, line := range seaMonster {
		padding := ""
		for i := 0; i < count; i++ {
			padding += "."
		}
		paddedMonster = append(paddedMonster, padding+line)
	}

	return
}
