package pocket

import (
	"fmt"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/utilities"
	"math"
)

type (
	Grid struct {
		dimensions *Dimensions
		Cells      map[Position]Cell
		edges      *Edges
	}

	Dimensions struct {
		X, Y Dimension
	}

	Dimension struct {
		Max, Min int
	}

	Position struct {
		X, Y int
	}

	Transformation string
)

const (
	FlipX            = "FlipX"
	FlipY            = "FlipY"
	Half             = "Half"
	Clockwise        = "Clockwise"
	CounterClockwise = "CounterClockwise"
)

func (grid Grid) Y() Dimension { return grid.Dimensions().Y }
func (grid Grid) X() Dimension { return grid.Dimensions().X }

func EmptyGrid() Grid {
	return Grid{
		Cells: map[Position]Cell{},
	}
}

func NewGrid(input []string) Grid {
	layout := Grid{
		Cells: map[Position]Cell{},
	}
	for rowIndex, row := range input {
		for colIndex, col := range row {
			if cell := NewCell(col); cell.IsActive() {
				layout.Cells[Position{X: colIndex, Y: rowIndex}] = cell
			}
		}
	}

	return layout
}

func (grid Grid) Copy() Grid {
	copyLayout := EmptyGrid()
	for pos, cell := range grid.Cells {
		copyLayout.Cells[pos] = cell
	}

	return copyLayout
}

func (grid *Grid) Dimensions() Dimensions {
	if grid.dimensions != nil {
		return *grid.dimensions
	}
	minX, minY := grid.Min()
	maxX, maxY := grid.Max()
	return Dimensions{
		X: Dimension{Min: minX, Max: maxX},
		Y: Dimension{Min: minY, Max: maxY},
	}
}

func (grid Grid) Min() (x, y int) {
	x = math.MaxInt32
	y = math.MaxInt32
	for position, _ := range grid.Cells {
		x = utilities.Min(x, position.X)
		y = utilities.Min(y, position.Y)
	}
	return
}

func (grid Grid) Max() (x, y int) {
	x = math.MinInt32
	y = math.MinInt32
	for position, _ := range grid.Cells {
		x = utilities.Max(x, position.X)
		y = utilities.Max(y, position.Y)
	}
	return
}

func (grid Grid) String() (str string) {
	for y := grid.Y().Min; y <= grid.Y().Max; y++ {
		for x := grid.X().Min; x <= grid.X().Max; x++ {
			if cell, ok := grid.Cells[Position{X: x, Y: y}]; ok {
				str += cell.String()
			} else {
				str += "."
			}
		}
		str += "\n"
	}
	return
}

func (grid Grid) StringWithDimensions(dimensions Dimensions) (str string) {
	for y := dimensions.Y.Min; y <= dimensions.Y.Max; y++ {
		for x := dimensions.X.Min; x <= dimensions.X.Max; x++ {
			if cell, ok := grid.Cells[Position{X: x, Y: y}]; ok {
				str += cell.String()
			} else {
				str += "."
			}
		}
		if y < dimensions.Y.Max-1 {
			str += "\n"
		}
	}
	return
}

func (grid Grid) Equals(other Grid) bool {
	for position, cell := range grid.Cells {
		if otherCell, ok := other.Cells[position]; !ok || otherCell != cell {
			return false
		}
	}

	return true
}

func (grid Grid) CountOccupied() (count int) {
	for _, cell := range grid.Cells {
		if cell == active {
			count++
		}
	}

	return
}

func (position Position) String() string {
	return fmt.Sprintf("[%d, %d]", position.X, position.Y)
}

func (grid Grid) Iterate(cellIteration func(cell Cell, grid Grid, position Position) Cell) Grid {
	nextGrid := EmptyGrid()
	for y := grid.Y().Min - 1; y <= grid.Y().Max+1; y++ {
		for x := grid.X().Min - 1; x <= grid.X().Max+1; x++ {
			var newCell Cell
			position := Position{X: x, Y: y}
			if cell, ok := grid.Cells[position]; ok {
				newCell = cellIteration(cell, grid, position)
			} else {
				newCell = cellIteration(InactiveCell(), grid, position)
			}
			if newCell.IsActive() {
				nextGrid.Cells[position] = newCell
			}
		}
	}

	return nextGrid
}

func (grid Grid) Edge(position EdgePosition) Edge {
	switch position {
	case Top:
		return grid.Edges().Top
	case Bottom:
		return grid.Edges().Bottom
	case Left:
		return grid.Edges().Left
	case Right:
		return grid.Edges().Right
	}

	return ""
}

func (grid *Grid) Edges() Edges {
	if grid.edges == nil {
		edges := Edges{}
		// TOP
		edge := ""
		y := 0
		for x := 0; x <= grid.X().Max; x++ {
			if cell, ok := grid.Cells[Position{X: x, Y: y}]; ok {
				edge += cell.String()
			} else {
				edge += "."
			}
		}
		edges.Top = Edge(edge)

		// BOTTOM
		edge = ""
		y = grid.Y().Max
		for x := 0; x <= grid.X().Max; x++ {
			if cell, ok := grid.Cells[Position{X: x, Y: y}]; ok {
				edge += cell.String()
			} else {
				edge += "."
			}
		}
		edges.Bottom = Edge(edge)

		// LEFT
		edge = ""
		x := 0
		for y := 0; y <= grid.Y().Max; y++ {
			if cell, ok := grid.Cells[Position{X: x, Y: y}]; ok {
				edge += cell.String()
			} else {
				edge += "."
			}
		}
		edges.Left = Edge(edge)

		// RIGHT
		edge = ""
		x = grid.X().Max
		for y := 0; y <= grid.Y().Max; y++ {
			if cell, ok := grid.Cells[Position{X: x, Y: y}]; ok {
				edge += cell.String()
			} else {
				edge += "."
			}
		}
		edges.Right = Edge(edge)

		grid.edges = &edges
	}

	return *grid.edges
}

func (grid Grid) Transform(transformation Transformation) Grid {
	maxY := grid.Y().Max
	maxX := grid.X().Max
	var alterations map[Transformation]func(position Position) Position
	alterations = map[Transformation]func(position Position) Position{
		FlipX: func(position Position) Position {
			return Position{X: position.X, Y: maxY - position.Y}
		},
		FlipY: func(position Position) Position {
			return Position{X: maxX - position.X, Y: position.Y}
		},
		Clockwise: func(position Position) Position {
			return Position{X: maxX - position.Y, Y: position.X}
		},
		CounterClockwise: func(position Position) Position {
			return Position{X: position.Y, Y: maxY - position.X}
		},
		Half: func(position Position) Position {
			return alterations[Clockwise](alterations[Clockwise](position)) // Twice clockwise
		},
	}

	newGrid := EmptyGrid()
	for position, cell := range grid.Cells {
		newGrid.Cells[alterations[transformation](position)] = cell
	}

	return newGrid
}

func (grid Grid) TrimEdges() Grid {
	newGrid := EmptyGrid()
	for position, cell := range grid.Cells {
		if position.Y == grid.Dimensions().Y.Min || position.Y == grid.Dimensions().Y.Max ||
			position.X == grid.Dimensions().X.Min || position.X == grid.Dimensions().X.Max {
			continue
		}
		newGrid.Cells[Position{X: position.X - 1, Y: position.Y - 1}] = cell
	}

	return newGrid
}
