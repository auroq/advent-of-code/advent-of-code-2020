package pocket

import (
	"math"
	"strings"
)

type (
	Image struct {
		Grids           map[string]Grid
		edges           []string
		corners         []string
		edgeMatches     map[string][]EdgeMatch
		transformations map[string][]Transformation
		positions       map[Position]string
		assembled       *Grid
	}
	EdgeMatch struct {
		matchId       string
		Position      EdgePosition
		MatchPosition EdgePosition
	}
)

func NewImage(input []string) *Image {
	return &Image{
		Grids:       parseGrids(input),
		edges:       nil,
		corners:     nil,
		edgeMatches: nil,
	}
}

func (image *Image) EdgeMatches() map[string][]EdgeMatch {
	if image.edgeMatches == nil {
		image.edgeMatches = matchEdges(getEdgesMap(image.Grids))
	}

	return image.edgeMatches
}

func (image *Image) Edges() []string {
	if image.edges == nil {
		for id, edgeMatches := range image.EdgeMatches() {
			if len(edgeMatches) == 3 {
				image.corners = append(image.edges, id)
			}
		}
	}

	return image.edges
}

func (image *Image) Corners() []string {
	if image.corners == nil {
		for id, edgeMatches := range image.EdgeMatches() {
			if len(edgeMatches) == 2 {
				image.corners = append(image.corners, id)
			}
		}
	}

	return image.corners
}

func parseGrids(input []string) (grids map[string]Grid) {
	grids = map[string]Grid{}

	isHeader := true
	var header string
	var lines []string
	if input[len(input)-1] != "" {
		input = append(input, "")
	}
	for _, line := range input {
		if line == "" {
			grids[header] = NewGrid(lines)
			isHeader = true
			lines = []string{}
		} else if isHeader {
			id := strings.Split(line, " ")[1]
			header = id[:len(id)-1]
			isHeader = false
		} else {
			lines = append(lines, line)
		}
	}

	return
}

func getEdgesMap(grids map[string]Grid) map[string]Edges {
	gridEdges := map[string]Edges{}
	for id, grid := range grids {
		gridEdges[id] = grid.Edges()
	}
	return gridEdges
}

func matchEdges(edgesMap map[string]Edges) (matches map[string][]EdgeMatch) {
	matches = map[string][]EdgeMatch{}
	for firstID, firstEdges := range edgesMap {
		for secondId, secondEdges := range edgesMap {
			if secondId == firstID {
				continue
			}
			matches = tryAddMatch(matches, firstID, secondId, firstEdges.Top, Top, secondEdges)
			matches = tryAddMatch(matches, firstID, secondId, firstEdges.Bottom, Bottom, secondEdges)
			matches = tryAddMatch(matches, firstID, secondId, firstEdges.Left, Left, secondEdges)
			matches = tryAddMatch(matches, firstID, secondId, firstEdges.Right, Right, secondEdges)
		}
	}

	return matches
}

func isEdgeMatch(edge Edge, otherEdges Edges) (bool, EdgePosition) {
	if edge.Equals(otherEdges.Top) {
		return true, Top
	}
	if edge.Equals(otherEdges.Bottom) {
		return true, Bottom
	}
	if edge.Equals(otherEdges.Left) {
		return true, Left
	}
	if edge.Equals(otherEdges.Right) {
		return true, Right
	}

	return false, ""
}

func tryAddMatch(
	matches map[string][]EdgeMatch,
	firstID, secondID string,
	firstEdge Edge,
	firstEdgePosition EdgePosition,
	secondEdges Edges,
) map[string][]EdgeMatch {
	if isMatch, position := isEdgeMatch(firstEdge, secondEdges); isMatch {
		matches[firstID] = append(matches[firstID], EdgeMatch{
			Position:      firstEdgePosition,
			matchId:       secondID,
			MatchPosition: position,
		})
	}

	return matches
}

func (image *Image) SetPositions(positions map[Position]string) {
	image.positions = positions
}

func (image *Image) Positions() map[Position]string {
	if image.positions == nil {
		ids := map[string]bool{}
		for id := range image.Grids {
			ids[id] = true
		}
		topLeft := ""
		for _, corner := range image.Corners() {
			if topLeft == "" || corner < topLeft {
				topLeft = corner
			}
		}

		positions := map[Position]string{}
		positions[Position{0, 0}] = topLeft
		delete(ids, topLeft)

		last := positions[Position{0, 0}]
		y := 0
		x := 1
		maxX := -1
		for len(ids) > 0 {
			xMatches := image.EdgeMatches()[last]
			var match string
			if y == 0 {
				for _, xMatch := range xMatches {
					if _, ok := ids[xMatch.matchId]; !ok {
						continue
					}
					if len(image.EdgeMatches()[xMatch.matchId]) > 3 {
						continue
					}
					match = xMatch.matchId
					break
				}
			} else {
			outer:
				for _, xMatch := range xMatches {
					if _, ok := ids[xMatch.matchId]; !ok {
						continue
					}
					for _, yMatch := range image.EdgeMatches()[positions[Position{x, y - 1}]] {
						if matchId := xMatch.matchId; matchId == yMatch.matchId {
							match = xMatch.matchId
							break outer
						}
					}
				}
			}
			positions[Position{x, y}] = match
			delete(ids, match)
			if (len(image.EdgeMatches()[match]) == 2 && maxX <= 0) || (y > 0 && x == maxX) {
				last = positions[Position{0, y}]
				y++
				maxX = x
				x = 0
			} else {
				last = match
				x++
			}
		}
		image.positions = positions
	}

	return image.positions
}

func (image *Image) Transformations() map[string][]Transformation {
	if image.transformations == nil {
		image.transformations = map[string][]Transformation{}
		for position, id := range image.Positions() {
			above := image.Positions()[Position{X: position.X, Y: position.Y - 1}]
			below := image.Positions()[Position{X: position.X, Y: position.Y + 1}]
			left := image.Positions()[Position{X: position.X - 1, Y: position.Y}]
			right := image.Positions()[Position{X: position.X + 1, Y: position.Y}]
			transformations := image.getTransformations(id, above, below, left, right)

			image.transformations[id] = transformations
		}
	}

	return image.transformations
}

func (image Image) getTransformations(id, above, below, left, right string) []Transformation {
	gridMatches := image.EdgeMatches()[id]
	mappings := EdgePositionTargets{
		Top:    Top,
		Bottom: Bottom,
		Left:   Left,
		Right:  Right,
	}
	for _, match := range gridMatches {
		var targetPosition EdgePosition
		switch match.matchId {
		case above:
			targetPosition = Top
		case below:
			targetPosition = Bottom
		case left:
			targetPosition = Left
		case right:
			targetPosition = Right
		}
		mappings.SetMapping(match.Position, targetPosition)
	}
	return determineTransformations(mappings)
}

func determineTransformations(mapping EdgePositionTargets) []Transformation {
	if (mapping.Top == Bottom || mapping.Bottom == Top) && (mapping.Left == Right || mapping.Right == Left) {
		return []Transformation{Half}
	}
	if mapping.Top == Bottom || mapping.Bottom == Top {
		return []Transformation{FlipX}
	}
	if mapping.Left == Right || mapping.Right == Left {
		return []Transformation{FlipY}
	}
	if (mapping.Top == Left || mapping.Bottom == Right) && (mapping.Left == Top || mapping.Right == Bottom) {
		return []Transformation{CounterClockwise, FlipX}
	}
	if (mapping.Top == Left || mapping.Bottom == Right) && (mapping.Left == Bottom || mapping.Right == Top) {
		return []Transformation{CounterClockwise}
	}
	if (mapping.Top == Right || mapping.Bottom == Left) && (mapping.Left == Top || mapping.Right == Bottom) {
		return []Transformation{Clockwise}
	}
	if (mapping.Top == Right || mapping.Bottom == Left) && (mapping.Left == Bottom || mapping.Right == Top) {
		return []Transformation{Clockwise, FlipX}
	}
	return nil
}

func (image *Image) Assembled() Grid {
	if image.assembled == nil {
		dimensions := Dimensions{
			X: Dimension{
				Min: 0,
				Max: math.MinInt32,
			},
			Y: Dimension{
				Min: 0,
				Max: math.MinInt32,
			},
		}
		assembledGrids := map[Position]Grid{}
		for position, id := range image.Positions() {
			grid := image.Grids[id]

			if position.X > dimensions.X.Max {
				dimensions.X.Max = position.X
			}
			if position.Y > dimensions.Y.Max {
				dimensions.Y.Max = position.Y
			}

			for _, transformation := range image.Transformations()[id] {
				grid = grid.Transform(transformation)
			}
			assembledGrids[position] = grid.TrimEdges()
		}

		newGrid := GroupGrids(assembledGrids, dimensions)
		image.assembled = &newGrid
	}

	return *image.assembled
}

func GroupGrids(grids map[Position]Grid, dimensions Dimensions) Grid {
	var lines []string
	lineOffset := 0
	for y := dimensions.Y.Min; y <= dimensions.Y.Max; y++ {
		maxGridLines := math.MinInt32
		for x := dimensions.X.Min; x <= dimensions.X.Max; x++ {
			grid := grids[Position{X: x, Y: y}]
			gridLines := strings.Split(grid.String(), "\n")
			if maxGridLines < len(gridLines) {
				maxGridLines = len(gridLines)
			}
			requiredLines := len(gridLines) + lineOffset
			for len(lines) < requiredLines {
				lines = append(lines, "")
			}
			for i, gridLine := range gridLines {
				lines[i+lineOffset] += gridLine
			}
		}
		lineOffset += maxGridLines - 1
	}
	return NewGrid(lines)
}
