package pocket

type (
	EdgePosition string

	Edge  string
	Edges struct {
		Top    Edge
		Bottom Edge
		Left   Edge
		Right  Edge
	}
	EdgePositionTargets struct {
		Top    EdgePosition
		Bottom EdgePosition
		Left   EdgePosition
		Right  EdgePosition
	}
)

const (
	Top    = "Top"
	Bottom = "Bottom"
	Left   = "Left"
	Right  = "Right"
)

func (edge Edge) Equals(other Edge) bool {
	return other == edge || other.Reverse() == edge
}

func (edge Edge) StrictEquals(other Edge) bool {
	return other == edge
}

func (edge Edge) Reverse() Edge {
	rns := []rune(edge)
	for i, j := 0, len(rns)-1; i < j; i, j = i+1, j-1 {
		rns[i], rns[j] = rns[j], rns[i]
	}

	return Edge(rns)
}

func (edgePosition EdgePosition) Inverse() EdgePosition {
	return map[EdgePosition]EdgePosition{
		Top:    Bottom,
		Bottom: Top,
		Left:   Right,
		Right:  Left,
	}[edgePosition]
}

func (edgePositionTargets *EdgePositionTargets) SetMapping(from EdgePosition, to EdgePosition) {
	switch from {
	case Top:
		edgePositionTargets.Top = to
	case Bottom:
		edgePositionTargets.Bottom = to
	case Left:
		edgePositionTargets.Left = to
	case Right:
		edgePositionTargets.Right = to
	}
}
