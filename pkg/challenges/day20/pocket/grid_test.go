package pocket_test

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/challenges/day20/pocket"
	"strconv"
	"strings"
	"testing"
)

func TestNewLayout(t *testing.T) {
	var tests = []struct {
		lines         []string
		expectedSeats map[pocket.Position]pocket.Cell
	}{
		{
			lines: []string{
				"###",
				"###",
				"###",
			},
			expectedSeats: map[pocket.Position]pocket.Cell{
				pocket.Position{X: 0, Y: 0}: pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 0}: pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 0}: pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 1}: pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 1}: pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 1}: pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 2}: pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 2}: pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 2}: pocket.ActiveCell(),
			},
		},
		{
			lines: []string{
				"...",
				".#.",
				"...",
			},
			expectedSeats: map[pocket.Position]pocket.Cell{
				pocket.Position{X: 1, Y: 1}: pocket.ActiveCell(),
			},
		},
		{
			lines: []string{
				"...",
				"...",
				"...",
			},
			expectedSeats: map[pocket.Position]pocket.Cell{},
		},
	}
	for i, test := range tests {
		test := test
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			t.Parallel()
			actual := pocket.NewGrid(test.lines)
			if len(actual.Cells) != len(test.expectedSeats) {
				t.Fatalf("length expected: %d, actual: %d", len(test.expectedSeats), len(actual.Cells))
			}
			for position, expectedSeat := range test.expectedSeats {
				if actualSeat, ok := actual.Cells[position]; !ok || actualSeat != expectedSeat {
					t.Errorf("expected position: %s not found in actual", position)
				} else if actualSeat != expectedSeat {
					t.Fatalf("position: %s, expected: %s, actual: %s", position, expectedSeat, actualSeat)
				}
			}
		})
	}
}

func TestLayout_String(t *testing.T) {
	var tests = []struct {
		startingSeats map[pocket.Position]pocket.Cell
		expected      []string
	}{
		{
			map[pocket.Position]pocket.Cell{
				pocket.Position{X: 1, Y: 0}: pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 1}: pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 2}: pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 2}: pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 2}: pocket.ActiveCell(),
			},
			[]string{
				".#.",
				"..#",
				"###",
			}},
		{
			map[pocket.Position]pocket.Cell{
				pocket.Position{X: 0, Y: 1}: pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 2}: pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 1}: pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 2}: pocket.ActiveCell(),
			},
			[]string{
				"#.#",
				".##",
				"",
			},
		},
	}
	for i, test := range tests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			layout := pocket.EmptyGrid()
			layout.Cells = test.startingSeats
			expected := strings.Trim(strings.Join(test.expected, "\n"), "\r\n ")
			actual := strings.Trim(layout.String(), "\r\n ")
			if actual != expected {
				t.Fatalf("expected:\n%s\n\nactual:\n%s", expected, actual)
			}
		})
	}
}
func TestIterate(t *testing.T) {
	initial := []string{
		"...",
		".#.",
		"...",
	}
	iterations := [][]string{
		{
			"###",
			"###",
			"###",
		},
		{
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
		},
	}
	grid := pocket.NewGrid(initial)
	for i, iteration := range iterations {
		expected := strings.Join(iteration, "\n")
		grid = grid.Iterate(func(cell pocket.Cell, grid pocket.Grid, position pocket.Position) pocket.Cell {
			return pocket.ActiveCell()
		})
		actual := grid.String()
		if strings.Trim(actual, "\r\n ") != strings.Trim(expected, "\r\n ") {
			t.Fatalf("iteration %d: expected:\n%s\n\nactual:\n%s", i, expected, actual)
		}
	}
}

func TestGrid_GetEdges(t *testing.T) {
	grid := pocket.EmptyGrid()
	grid.Cells = map[pocket.Position]pocket.Cell{
		pocket.Position{X: 1, Y: 0}: pocket.ActiveCell(),
		pocket.Position{X: 2, Y: 1}: pocket.ActiveCell(),
		pocket.Position{X: 0, Y: 2}: pocket.ActiveCell(),
		pocket.Position{X: 1, Y: 2}: pocket.ActiveCell(),
		pocket.Position{X: 2, Y: 2}: pocket.ActiveCell(),
	}
	expected := pocket.Edges{
		Top:    ".#.",
		Bottom: "###",
		Left:   "..#",
		Right:  ".##",
	}
	actual := grid.Edges()
	if !actual.Top.Equals(expected.Top) {
		t.Fatalf("Top edge expected: %s actual: %s", expected.Top, actual.Top)
	}
	if !actual.Bottom.Equals(expected.Bottom) {
		t.Fatalf("Bottom edge expected: %s actual: %s", expected.Bottom, actual.Bottom)
	}
	if !actual.Left.Equals(expected.Left) {
		t.Fatalf("Left edge expected: %s actual: %s", expected.Left, actual.Left)
	}
	if !actual.Right.Equals(expected.Right) {
		t.Fatalf("Right edge expected: %s actual: %s", expected.Right, actual.Right)
	}
}

func TestGrid_Transform(t *testing.T) {
	startingGrid := pocket.EmptyGrid()
	startingGrid.Cells = map[pocket.Position]pocket.Cell{
		pocket.Position{X: 1, Y: 0}: pocket.ActiveCell(),
		pocket.Position{X: 2, Y: 1}: pocket.ActiveCell(),
		pocket.Position{X: 0, Y: 2}: pocket.ActiveCell(),
		pocket.Position{X: 1, Y: 2}: pocket.ActiveCell(),
		pocket.Position{X: 2, Y: 2}: pocket.ActiveCell(),
	}
	tests := []struct {
		rotation pocket.Transformation
		expected map[pocket.Position]pocket.Cell
	}{
		{
			rotation: pocket.FlipX,
			expected: map[pocket.Position]pocket.Cell{
				pocket.Position{X: 1, Y: 2}: pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 1}: pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 0}: pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 0}: pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 0}: pocket.ActiveCell(),
			},
		},
		{
			rotation: pocket.FlipY,
			expected: map[pocket.Position]pocket.Cell{
				pocket.Position{X: 1, Y: 0}: pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 1}: pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 2}: pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 2}: pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 2}: pocket.ActiveCell(),
			},
		},
		{
			rotation: pocket.Clockwise,
			expected: map[pocket.Position]pocket.Cell{
				pocket.Position{X: 1, Y: 2}: pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 1}: pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 0}: pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 1}: pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 2}: pocket.ActiveCell(),
			},
		},
		{
			rotation: pocket.CounterClockwise,
			expected: map[pocket.Position]pocket.Cell{
				pocket.Position{X: 0, Y: 1}: pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 0}: pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 2}: pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 1}: pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 0}: pocket.ActiveCell(),
			},
		},
		{
			rotation: pocket.Half,
			expected: map[pocket.Position]pocket.Cell{
				pocket.Position{X: 1, Y: 2}: pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 1}: pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 0}: pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 0}: pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 0}: pocket.ActiveCell(),
			},
		},
	}
	for _, test := range tests {
		test := test
		expected := pocket.EmptyGrid()
		expected.Cells = test.expected
		actual := startingGrid.Transform(test.rotation)
		if !actual.Equals(expected) {
			t.Fatalf("\ninitial:\n%s\n\nexpected:\n%s\n\nactual:\n%s", startingGrid, expected, actual)
		}
	}
}

func TestGrid_TrimEdges(t *testing.T) {
	startingGrid := pocket.NewGrid([]string{
		"#####",
		"#####",
		"#####",
		"#####",
		"#####",
	})
	expected := pocket.NewGrid([]string{
		"###",
		"###",
		"###",
	})
	actual := startingGrid.TrimEdges()
	if !actual.Equals(expected) {
		t.Fatalf("\ninitial:\n%s\nexpected:\n%s\nactual:\n%s", startingGrid, expected, actual)
	}
}
