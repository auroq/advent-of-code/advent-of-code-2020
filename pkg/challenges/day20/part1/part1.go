package part1

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/challenges/day20/pocket"
	"strconv"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	image := pocket.NewImage(input)

	answer = 1
	for _, corner := range image.Corners() {
		val, err := strconv.Atoi(corner)
		if err != nil {
			return -1, err
		}
		answer *= val
	}

	return
}
