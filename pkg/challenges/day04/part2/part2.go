package part2

import (
	"encoding/json"
	"strconv"
	"strings"
)

type Part2 struct{}

type Passport struct {
	Byr string `json:"byr"`
	Cid string `json:"cid"`
	Ecl string `json:"ecl"`
	Eyr string `json:"eyr"`
	Hcl string `json:"hcl"`
	Hgt string `json:"hgt"`
	Iyr string `json:"iyr"`
	Pid string `json:"pid"`
}

func NewPassportFromJson(jsonString string) (passport Passport, err error) {
	err = json.Unmarshal([]byte(jsonString), &passport)
	return
}

func (part Part2) Run(input []string) (answer int, err error) {
	var lines string
	for i, line := range input {
		if i == len(input)-1 {
			lines = line
			line = ""
		}
		if line == "" {
			passport, err := NewPassportFromJson(linesToJson(lines))
			if err != nil {
				return -1, err
			}
			if passport.isValid() {
				answer++
			}
			lines = ""
		} else {
			lines += " " + line
		}
	}
	return
}

func linesToJson(lines string) string {
	lines = strings.TrimSpace(lines)
	lines = strings.ReplaceAll(lines, " ", "\", \"")
	lines = strings.ReplaceAll(lines, ":", "\": \"")
	lines = "{\"" + lines + "\"}"

	return lines
}

func (passport Passport) isValid() bool {
	return passportNotEmpty(passport) &&
		byrIsValid(passport.Byr) &&
		iyrIsValid(passport.Iyr) &&
		eyrIsValid(passport.Eyr) &&
		hgtIsValid(passport.Hgt) &&
		hclIsValid(passport.Hcl) &&
		eclIsValid(passport.Ecl) &&
		pidIsValid(passport.Pid)
}

func passportNotEmpty(passport Passport) bool {
	return passport.Byr != "" &&
		passport.Ecl != "" &&
		passport.Eyr != "" &&
		passport.Hcl != "" &&
		passport.Hgt != "" &&
		passport.Iyr != "" &&
		passport.Pid != ""
}

func pidIsValid(value string) bool {
	return len(value) == 9
}

func eclIsValid(value string) (valid bool) {
	validColors := []string{"amb", "blu", "brn", "gry", "grn", "hzl", "oth"}
	for _, color := range validColors {
		if value == color {
			valid = true
		}
	}

	return
}

func hclIsValid(value string) (valid bool) {
	if valid = strings.HasPrefix(value, "#"); !valid {
		return
	}
	if valid = len(value) == 7; !valid {
		return
	}
	validColorChar := []rune{'a', 'b', 'c', 'd', 'e', 'f', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}
	for _, h := range value[1:] {
		validColor := false
		for _, c := range validColorChar {
			if rune(h) == c {
				validColor = true
			}
		}
		if !validColor {
			return false
		}
	}

	return true
}

func hgtIsValid(value string) bool {
	if strings.HasSuffix(value, "cm") {
		hgt, err := strconv.Atoi(value[:len(value)-2])
		if err != nil {
			return false
		}
		return hgt >= 150 && hgt <= 193
	} else if strings.HasSuffix(value, "in") {
		hgt, err := strconv.Atoi(value[:len(value)-2])
		if err != nil {
			return false
		}
		return hgt >= 59 && hgt <= 76
	} else {
		return false
	}
}

func byrIsValid(value string) bool {
	return dateIsValid(value, 1920, 2002)
}

func iyrIsValid(value string) bool {
	return dateIsValid(value, 2010, 2020)
}

func eyrIsValid(value string) bool {
	return dateIsValid(value, 2020, 2030)
}

func dateIsValid(value string, min, max int) bool {
	if len(value) != 4 {
		return false
	}
	byr, err := strconv.Atoi(value)
	if err != nil {
		return false
	}
	return byr >= min && byr <= max
}
