package part2

import (
	"strconv"
	"strings"
	"testing"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{
		// Invalid
		"eyr:1972 cid:100",
		"hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926",
		"",
		"iyr:2019",
		"hcl:#602927 eyr:1967 hgt:170cm",
		"ecl:grn pid:012533040 byr:1946",
		"",
		"hcl:dab227 iyr:2012",
		"ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277",
		"",
		"hgt:59cm ecl:zzz",
		"eyr:2038 hcl:74454a iyr:2023",
		"pid:3556412378 byr:2007",
		"",
		//Valid
		"pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980",
		"hcl:#623a2f",
		"",
		"eyr:2029 ecl:blu cid:129 byr:1989",
		"iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm",
		"",
		"hcl:#888785",
		"hgt:164cm byr:2001 iyr:2015 cid:88",
		"pid:545766238 ecl:hzl",
		"eyr:2022",
		"",
		"iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719",
	}
	expected := 4
	part1 := Part2{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestIsValid(t *testing.T) {
	var tests = []struct {
		lines []string
		valid bool
	}{
		{
			// Invalid
			[]string{
				"eyr:1972 cid:100",
				"hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926",
			}, false}, {
			[]string{
				"iyr:2019",
				"hcl:#602927 eyr:1967 hgt:170cm",
				"ecl:grn pid:012533040 byr:1946",
			}, false}, {
			[]string{
				"hcl:dab227 iyr:2012",
				"ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277",
			}, false}, {
			[]string{
				"hgt:59cm ecl:zzz",
				"eyr:2038 hcl:74454a iyr:2023",
				"pid:3556412378 byr:2007",
			}, false}, {
			// Valid
			[]string{
				"pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980",
				"hcl:#623a2f",
			}, true}, {
			[]string{
				"eyr:2029 ecl:blu cid:129 byr:1989",
				"iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm",
			}, true}, {
			[]string{
				"hcl:#888785",
				"hgt:164cm byr:2001 iyr:2015 cid:88",
				"pid:545766238 ecl:hzl",
				"eyr:2022",
			}, true}, {
			[]string{
				"iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719",
			}, true},
	}
	for i, test := range tests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			passport, err := NewPassportFromJson(linesToJson(strings.Join(test.lines, " ")))
			if err != nil {
				t.Error(err)
			}
			actual := passport.isValid()
			expected := test.valid
			if actual != expected {
				t.Fatalf("expected: %t, actual: %t", expected, actual)
			}
		})
	}
}

func TestValidity(t *testing.T) {
	var tests = []struct {
		name  string
		value string
		fun   func(value string) bool
		valid bool
	}{
		{"byr-val", "2002", byrIsValid, true},
		{"byr-inv-low", "1919", byrIsValid, false},
		{"byr-inv-high", "2003", byrIsValid, false},
		{"eyr-val", "2025", eyrIsValid, true},
		{"eyr-inv-low", "2019", eyrIsValid, false},
		{"eyr-inv-high", "2031", eyrIsValid, false},
		{"iyr-val", "2015", iyrIsValid, true},
		{"iyr-inv-low", "2009", iyrIsValid, false},
		{"iyr-inv-high", "2021", iyrIsValid, false},
		{"hgt-val-in", "60in", hgtIsValid, true},
		{"hgt-val-cm", "190cm", hgtIsValid, true},
		{"hgt-inv-in", "190in", hgtIsValid, false},
		{"hgt-inv-cm", "200cm", hgtIsValid, false},
		{"hgt-inv-empty", "190", hgtIsValid, false},
		{"hcl-val", "#123abc", hclIsValid, true},
		{"hcl-inv-let", "#123abz", hclIsValid, false},
		{"hcl-inv-hsh", "123abc", hclIsValid, false},
		{"ecl-val", "brn", eclIsValid, true},
		{"ecl-inv", "wat", eclIsValid, false},
		{"pid-val", "000000001", pidIsValid, true},
		{"pid-inv-long", "0123456789", pidIsValid, false},
		{"pid-inv-short", "0123", pidIsValid, false},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			expected := test.valid
			actual := test.fun(test.value)
			if actual != expected {
				t.Fatalf("expected: %t, actual: %t", expected, actual)
			}
		})
	}
}
