package part1

import (
	"strconv"
	"testing"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"ecl:gry pid:860033327 eyr:2020 hcl:#fffffd",
		"byr:1937 iyr:2017 cid:147 hgt:183cm",
		"",
		"iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884",
		"hcl:#cfa07d byr:1929",
		"",
		"hcl:#ae17e1 iyr:2013",
		"eyr:2024",
		"ecl:brn pid:760753108 byr:1931",
		"hgt:179cm",
		"",
		"hcl:#cfa07d eyr:2025 pid:166559648",
		"iyr:2011 ecl:brn hgt:59in",
	}
	expected := 2
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestIsValid(t *testing.T) {
	var tests = []struct {
		passport Passport
		valid    bool
	}{
		{
			NewPassport(
				"1937",
				"147",
				"gry",
				"2020",
				"#fffffd",
				"183cm",
				"2017",
				"860033327",
			), true,
		}, {
			NewPassport(
				"1929",
				"350",
				"amb",
				"2023",
				"#cfa07d",
				"",
				"2013",
				"02804884",
			), false,
		}, {
			NewPassport(
				"1931",
				"",
				"brn",
				"2024",
				"#ae17e1",
				"179cm",
				"2013",
				"760753108",
			), true},
		{
			NewPassport(
				"",
				"",
				"brn",
				"2025",
				"#cfa07d",
				"59in",
				"2011",
				"166559648",
			), false,
		},
	}
	for i, test := range tests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			actual := test.passport.isValid()
			expected := test.valid
			if actual != expected {
				t.Fatalf("expected: %t, actual: %t", expected, actual)
			}
		})
	}
}
