package part1

import (
	"encoding/json"
	"strings"
)

type Part1 struct{}

type Passport struct {
	Byr string `json:"byr"`
	Cid string `json:"cid"`
	Ecl string `json:"ecl"`
	Eyr string `json:"eyr"`
	Hcl string `json:"hcl"`
	Hgt string `json:"hgt"`
	Iyr string `json:"iyr"`
	Pid string `json:"pid"`
}

func NewPassport(byr, cid, ecl, eyr, hcl, hgt, iyr, pid string) Passport {
	return Passport{
		Byr: byr,
		Cid: cid,
		Ecl: ecl,
		Eyr: eyr,
		Hcl: hcl,
		Hgt: hgt,
		Iyr: iyr,
		Pid: pid,
	}
}

func (part Part1) Run(input []string) (answer int, err error) {
	var lines string
	for i, line := range input {
		if line == "" || i == len(input)-1 {
			lines = strings.TrimSpace(lines)
			lines = strings.ReplaceAll(lines, " ", "\", \"")
			lines = strings.ReplaceAll(lines, ":", "\": \"")
			lines = "{\"" + lines + "\"}"
			var passport Passport
			if err = json.Unmarshal([]byte(lines), &passport); err != nil {
				return
			}
			if passport.isValid() {
				answer++
			}
			lines = ""
		} else {
			lines += " " + line
		}
	}
	return
}

func (passport Passport) isValid() bool {
	return passport.Byr != "" &&
		//passport.Cid != "" &&
		passport.Ecl != "" &&
		passport.Eyr != "" &&
		passport.Hcl != "" &&
		passport.Hgt != "" &&
		passport.Iyr != "" &&
		passport.Pid != ""
}
