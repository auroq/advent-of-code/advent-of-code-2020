package part2

import (
	"strconv"
)

type Part2 struct{}

func (day Part2) Run(input []string) (answer int, err error) {
	for _, i := range input {
		for _, j := range input {
			for _, k := range input {
				x, err := strconv.Atoi(i)
				if err != nil {
					return -1, err
				}
				y, err := strconv.Atoi(j)
				if err != nil {
					return -1, err
				}
				z, err := strconv.Atoi(k)
				if err != nil {
					return -1, err
				}

				if x+y+z == 2020 {
					answer = x * y * z
					return answer, nil
				}
			}
		}
	}

	return
}
