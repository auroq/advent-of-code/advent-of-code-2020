package part2

import (
	"testing"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{
		"1721",
		"979",
		"366",
		"299",
		"675",
		"1456",
	}
	expected := 241861950
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}
