package part1

import (
	"strconv"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	for _, i := range input {
		for _, j := range input {
			x, err := strconv.Atoi(i)
			if err != nil {
				return -1, err
			}
			y, err := strconv.Atoi(j)
			if err != nil {
				return -1, err
			}

			if x+y == 2020 {
				answer = x * y
				return answer, nil
			}
		}
	}

	return
}
