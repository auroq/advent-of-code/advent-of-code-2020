package part1

import (
	"testing"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"1721",
		"979",
		"366",
		"299",
		"675",
		"1456",
	}
	expected := 514579
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}
