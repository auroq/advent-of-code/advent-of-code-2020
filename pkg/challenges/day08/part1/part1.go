package part1

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/bootcode"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	code, err := bootcode.NewInterpreter(input)
	if err != nil {
		return -1, err
	}
	answer, _ = code.Execute()
	return
}
