package part2

import (
	"errors"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/bootcode"
	"strings"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	for i := 0; i < len(input); i++ {
		lines, err_ := swap(input, i)
		if err_ != nil {
			continue
		}
		code, err := bootcode.NewInterpreter(lines)
		if err != nil {
			return -1, err
		}
		answer, err_ = code.Execute()
		if err_ != nil {
			continue
		}
		return answer, nil
	}
	return -1, errors.New("not found")
}

func swap(input []string, index int) ([]string, error) {
	lines := make([]string, len(input))
	copy(lines, input)
	start := lines[:index]
	end := lines[index+1:]
	val := lines[index]
	if strings.Contains(lines[index], "nop") {
		val = strings.ReplaceAll(lines[index], "nop", "jmp")
	} else if strings.Contains(lines[index], "jmp") {
		val = strings.ReplaceAll(lines[index], "jmp", "nop")
	} else {
		return nil, errors.New("not found")
	}
	return append(append(start, val), end...), nil
}
