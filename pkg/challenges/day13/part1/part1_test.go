package part1

import (
	"testing"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"939",
		"7,13,x,x,59,x,31,19",
	}
	expected := 295
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestEarliestBus(t *testing.T) {
	var buses = []int{7, 13, 59, 31, 19}
	expectedId, expectedTime := 59, 944
	actualId, actualTime := GetEarliestBus(buses, 939)
	if actualId != expectedId {
		t.Fatalf("ID expected: %d, actual: %d", expectedId, actualId)
	}
	if actualTime != expectedTime {
		t.Fatalf("time expected: %d, actual: %d", expectedTime, actualTime)
	}
}
