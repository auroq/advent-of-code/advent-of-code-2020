package part1

import (
	"math"
	"strconv"
	"strings"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	targetTime, err := strconv.Atoi(input[0])
	if err != nil {
		return -1, err
	}
	var buses []int
	for _, bus := range strings.Split(input[1], ",") {
		if busId, err := strconv.Atoi(bus); err == nil {
			buses = append(buses, busId)
		}
	}
	minId, minTime := GetEarliestBus(buses, targetTime)
	wait := minTime - targetTime
	answer = minId * wait
	return
}

func GetEarliestBus(buses []int, time int) (minId, minTime int) {
	possibleBusTime := map[int]int{}
	minBusId := -1
	minBusTime := math.MaxInt32
	for _, bus := range buses {
		possibleBusTime[bus] = bus
	}

	for len(possibleBusTime) > 1 {
		for busId, busTime := range possibleBusTime {
			nextBusTime := busTime + busId
			possibleBusTime[busId] = nextBusTime
			if nextBusTime >= time {
				if nextBusTime < minBusTime {
					minBusTime = nextBusTime
					minBusId = busId
				} else {
					delete(possibleBusTime, busId)
				}
			}
		}
	}

	return minBusId, minBusTime
}
