package part2

import (
	"strconv"
	"strings"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	var buses []int
	for _, bus := range strings.Split(input[1], ",") {
		if busId, err := strconv.Atoi(bus); err == nil {
			buses = append(buses, busId)
		} else {
			buses = append(buses, -1)
		}
	}
	answer = GetContestTime(buses)

	return
}

func GetContestTime(buses []int) (timestamp int) {
	offset := 1
	for i, bus := range buses {
		if bus < 0 {
			continue
		}
		for (timestamp+i)%bus != 0 {
			timestamp += offset
		}
		offset *= bus
	}

	return
}
