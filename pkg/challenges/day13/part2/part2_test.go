package part2

import (
	"strconv"
	"testing"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{
		"939",
		"7,13,x,x,59,x,31,19",
	}
	expected := 1068781
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestGetContestTime(t *testing.T) {
	var tests = []struct {
		buses    []int
		expected int
	}{
		{[]int{7, 13, -1, -1, 59, -1, 31, 19}, 1068781},
		{[]int{17, -1, 13, 19}, 3417},
		{[]int{67, 7, 59, 61}, 754018},
		{[]int{67, -1, 7, 59, 61}, 779210},
		{[]int{67, 7, -1, 59, 61}, 1261476},
		{[]int{1789, 37, 47, 1889}, 1202161486},
	}
	for _, test := range tests {
		t.Run(strconv.Itoa(int(test.expected)), func(t *testing.T) {
			actual := GetContestTime(test.buses)
			expected := test.expected
			if actual != expected {
				t.Fatalf("time expected: %d, actual: %d", expected, actual)
			}
		})
	}
}
