package part1

import (
	"strings"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	for _, line := range input {
		id := parsePass(line)
		if id > answer {
			answer = id
		}
	}
	return
}

func parsePass(input string) int {
	row := parseRow(input[:7])
	col := parseColumn(input[7:])
	return row*8 + col
}

func parseRow(input string) int {
	return parse(0, 127, 'f', true, input)
}

func parseColumn(input string) int {
	return parse(0, 7, 'l', false, input)
}

func parse(min, max int, minChar rune, keepMin bool, input string) int {
	for _, val := range strings.ToLower(input) {
		if val == minChar {
			max -= (max + 1 - min) / 2
		} else {
			min += (max + 1 - min) / 2
		}
	}

	if keepMin {
		return min
	}
	return max
}
