package part1

import (
	"testing"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"BFFFBBFRRR",
		"FFFBBBFRRR",
		"BBFFBBFRLL",
	}
	expected := 820
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestParseRow(t *testing.T) {
	input := "FBFBBFF"
	expected := 44
	actual := parseRow(input)
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestParseColumn(t *testing.T) {
	input := "RLR"
	expected := 5
	actual := parseColumn(input)
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestParseBoardingPass(t *testing.T) {
	var tests = []struct {
		pass     string
		expected int
	}{
		{"FBFBBFFRLR", 357},
		{"BFFFBBFRRR", 567},
		{"FFFBBBFRRR", 119},
		{"BBFFBBFRLL", 820},
	}
	for _, test := range tests {
		t.Run(test.pass, func(t *testing.T) {
			expected := test.expected
			actual := parsePass(test.pass)
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}
