package part2

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"testing"
)

func TestPart2_Run(t *testing.T) {
	var inputs []string
	expected := rand.Int63n(1024)
	for i := int64(0); i < 1024; i++ {
		if i != expected {
			inputs = append(inputs, seatNumToId(i))
		}
	}
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != int(expected) {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestGetIds(t *testing.T) {
	var inputs = []string{
		"BFFFBBFRRR",
		"FFFBBBFRRR",
		"BBFFBBFRLL",
	}
	expected := []int{119, 567, 820}
	actual := getIds(inputs)
	for i := range expected {
		if actual[i] != expected[i] {
			t.Fatalf("expected: %d, actual: %d", expected, actual)
		}
	}
}

func TestParseRow(t *testing.T) {
	input := "FBFBBFF"
	expected := 44
	actual := parseRow(input)
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestParseColumn(t *testing.T) {
	input := "RLR"
	expected := 5
	actual := parseColumn(input)
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestParseBoardingPass(t *testing.T) {
	var tests = []struct {
		pass     string
		expected int
	}{
		{"FFFFFFFLLL", 0},
		{"FFFFFFFLLR", 1},
		{"FFFFFFBLLL", 8},
		{"FBFBBFFRLR", 357},
		{"BFFFBBFRRR", 567},
		{"FFFBBBFRRR", 119},
		{"BBFFBBFRLL", 820},
	}
	for _, test := range tests {
		t.Run(test.pass, func(t *testing.T) {
			expected := test.expected
			actual := parsePass(test.pass)
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}

func seatNumToId(seatNum int64) string {
	bin := fmt.Sprintf("%010s", fmt.Sprint(strconv.FormatInt(seatNum, 2)))
	row := strings.ReplaceAll(strings.ReplaceAll(bin[:7], "0", "F"), "1", "B")
	col := strings.ReplaceAll(strings.ReplaceAll(bin[7:], "0", "L"), "1", "R")
	return row + col
}
