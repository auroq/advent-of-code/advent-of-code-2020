package part2

import (
	"sort"
	"strings"
)

type Part2 struct{}

func (day Part2) Run(input []string) (answer int, err error) {
	ids := getIds(input)
	lastId := ids[0]
	for _, id := range ids[1:] {
		if id != lastId+1 {
			answer = lastId + 1
			return
		}
		lastId = id
	}

	return
}

func getIds(input []string) (ids []int) {
	for _, line := range input {
		ids = append(ids, parsePass(line))
		sort.Ints(ids)
	}
	return
}

func parsePass(input string) int {
	row := parseRow(input[:7])
	col := parseColumn(input[7:])
	return row*8 + col
}

func parseRow(input string) int {
	return parse(0, 127, 'f', true, input)
}

func parseColumn(input string) int {
	return parse(0, 7, 'l', false, input)
}

func parse(min, max int, minChar rune, keepMin bool, input string) int {
	for _, val := range strings.ToLower(input) {
		if val == minChar {
			max -= (max + 1 - min) / 2
		} else {
			min += (max + 1 - min) / 2
		}
	}

	if keepMin {
		return min
	}
	return max
}
