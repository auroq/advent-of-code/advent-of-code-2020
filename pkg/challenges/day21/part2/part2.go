package part2

import (
	"fmt"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/utilities/datastructures"
	"sort"
	"strings"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	intersections := map[string]*datastructures.Set{}
	allFoods := datastructures.NewSet()
	foodCounts := map[string]int{}
	for _, line := range input {
		parts := strings.Split(line, " (contains ")
		foods := strings.Split(parts[0], " ")
		allergens := strings.Split(parts[1][:len(parts[1])-1], ", ")

		for _, food := range foods {
			// add all to all food
			allFoods.Add(food)
			if _, ok := foodCounts[food]; !ok {
				foodCounts[food] = 1
			} else {
				foodCounts[food] = foodCounts[food] + 1
			}
		}

		for _, allergen := range allergens {
			// right set is new from allergen
			rightSet := datastructures.NewStringSet(foods...)
			// if allergen didn't exist previously grab everything
			if _, ok := intersections[allergen]; !ok {
				intersections[allergen] = rightSet
				continue
			}
			newSet := datastructures.NewSet()
			// left set is existing
			leftSet := intersections[allergen].Copy()
			// add all from new that exist in current
			for _, food := range rightSet.ToSlice() {
				if leftSet.Contains(food) {
					newSet.Add(food)
				}
			}
			// add all from current that exist in new
			for _, food := range leftSet.ToSlice() {
				if rightSet.Contains(food) {
					newSet.Add(food)
				}
			}
			intersections[allergen] = newSet

			if newSet.Count() < 1 {
				err = fmt.Errorf("unexpected empty allergen set: %s", allergen)
				return
			}

			if newSet.Count() == 1 {
				item := newSet.ToSlice()[0]
				for otherAllergen, otherFood := range intersections {
					if otherAllergen == allergen {
						continue
					}
					if otherFood.Contains(item) {
						otherFood.Remove(item)
					}
				}
			}
		}
	}

	clean := allFoods.Copy()
	for _, foodList := range intersections {
		for _, food := range foodList.ToSlice() {
			if clean.Contains(food) {
				clean.Remove(food)
			}
		}
	}

	for _, food := range clean.ToSlice() {
		answer += foodCounts[food.(string)]
	}

	limit(intersections)

	var allergens []string
	for allergen := range intersections {
		allergens = append(allergens, allergen)
	}
	sort.Strings(allergens)

	answerStr := ""
	for _, allergen := range allergens {
		answerStr += intersections[allergen].ToSlice()[0].(string) + ","
	}
	answerStr = answerStr[:len(answerStr)-1]
	fmt.Println(answerStr)

	return
}

func limit(mp map[string]*datastructures.Set) {
	lst := TooMany(mp)
	counts := GetCounts(mp, lst)
	for len(lst) > 0 {
		for _, key := range lst {
			for _, food := range mp[key].ToSlice() {
				if counts[food.(string)] == 1 {
					mp[key] = datastructures.NewSet(food)
					break
				}
			}
		}
		lst = TooMany(mp)
		counts = GetCounts(mp, lst)
	}
}

func GetCounts(mp map[string]*datastructures.Set, keys []string) map[string]int {
	counts := map[string]int{}
	for _, key := range keys {
		for _, val := range mp[key].ToSlice() {
			if _, ok := counts[val.(string)]; !ok {
				counts[val.(string)] = 1
			} else {
				counts[val.(string)] = counts[val.(string)] + 1
			}
		}
	}

	return counts
}

func TooMany(mp map[string]*datastructures.Set) (tooMany []string) {
	for key, val := range mp {
		if val.Count() > 1 {
			tooMany = append(tooMany, key)
		}
	}

	return
}
