package part2

import (
	"testing"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{
		"mxmxvkd kfcds sqjhc nhms (contains dairy, fish)",
		"trh fvjkl sbzzf mxmxvkd (contains dairy)",
		"sqjhc fvjkl (contains soy)",
		"sqjhc mxmxvkd sbzzf (contains fish)",
	}
	expected := 5
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}
