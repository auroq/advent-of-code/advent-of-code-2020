package part1

import (
	"fmt"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/utilities/datastructures"
	"strings"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	intersections := map[string]*datastructures.Set{}
	allFoods := datastructures.NewSet()
	foodCounts := map[string]int{}
	for _, line := range input {
		parts := strings.Split(line, " (contains ")
		foods := strings.Split(parts[0], " ")
		allergens := strings.Split(parts[1][:len(parts[1])-1], ", ")

		for _, food := range foods {
			// add all to all food
			allFoods.Add(food)
			if _, ok := foodCounts[food]; !ok {
				foodCounts[food] = 1
			} else {
				foodCounts[food] = foodCounts[food] + 1
			}
		}

		for _, allergen := range allergens {
			// right set is new from allergen
			rightSet := datastructures.NewStringSet(foods...)
			// if allergen didn't exist previously grab everything
			if _, ok := intersections[allergen]; !ok {
				intersections[allergen] = rightSet
				continue
			}
			newSet := datastructures.NewSet()
			// left set is existing
			leftSet := intersections[allergen].Copy()
			// add all from new that exist in current
			for _, food := range rightSet.ToSlice() {
				if leftSet.Contains(food) {
					newSet.Add(food)
				}
			}
			// add all from current that exist in new
			for _, food := range leftSet.ToSlice() {
				if rightSet.Contains(food) {
					newSet.Add(food)
				}
			}
			intersections[allergen] = newSet
		}

		for allergen, foods := range intersections {
			if foods.Count() < 1 {
				err = fmt.Errorf("unexpected empty allergen set: %s", allergen)
				return
			}
		}
	}

	clean := allFoods.Copy()
	for _, foodList := range intersections {
		for _, food := range foodList.ToSlice() {
			if clean.Contains(food) {
				clean.Remove(food)
			}
		}
	}

	for _, food := range clean.ToSlice() {
		answer += foodCounts[food.(string)]
	}

	return
}
