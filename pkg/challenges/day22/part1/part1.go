package part1

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/utilities"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	var count int
	for i, line := range input {
		if line == "" {
			count = i
		}
	}
	for input[len(input)-1] == "" {
		input = input[:len(input)-1]
	}
	player1, err := utilities.AsToIs(input[1:count])
	if err != nil {
		return -1, err
	}
	player2, err := utilities.AsToIs(input[count+2:])
	if err != nil {
		return -1, err
	}
	_, finalDeck := playGame(player1, player2, 0)
	answer = score(finalDeck)

	return
}

func playGame(player1, player2 []int, round int) (int, []int) {
	if len(player1) == 0 {
		return round, player2
	}
	if len(player2) == 0 {
		return round, player1
	}

	left := player1[0]
	right := player2[0]
	if left > right {
		return playGame(
			append(player1[1:], []int{left, right}...),
			player2[1:],
			round+1,
		)
	} else {
		return playGame(
			player1[1:],
			append(player2[1:], []int{right, left}...),
			round+1,
		)
	}
}

func score(deck []int) (score int) {
	for i, card := range deck {
		score += card * (len(deck) - i)
	}

	return
}
