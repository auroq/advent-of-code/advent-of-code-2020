package part1

import (
	"testing"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"Player 1:",
		"9",
		"2",
		"6",
		"3",
		"1",
		"",
		"Player 2:",
		"5",
		"8",
		"4",
		"7",
		"10",
	}
	expected := 306
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestPlayGame(t *testing.T) {
	player1 := []int{9, 2, 6, 3, 1}
	player2 := []int{5, 8, 4, 7, 10}
	expectedRounds := 29
	expectedDeck := []int{3, 2, 10, 6, 8, 5, 9, 4, 7, 1}

	actualRounds, actualDeck := playGame(player1, player2, 0)

	t.Run("round count should be correct", func(t *testing.T) {
		actual := actualRounds
		expected := expectedRounds
		if actual != expected {
			t.Fatalf("expected: %d, actual: %d", expected, actual)
		}
	})

	t.Run("final deck should be correct", func(t *testing.T) {
		actual := actualDeck
		expected := expectedDeck
		if len(actual) != len(expected) {
			t.Fatalf("expected: %v, actual: %v", expected, actual)
		}
		for i := range expectedDeck {
			if actual[i] != expected[i] {
				t.Fatalf("differ at index: %d, expected: %v, actual: %v", i, expected, actual)
			}
		}
	})
}

func TestScore(t *testing.T) {
	deck := []int{3, 2, 10, 6, 8, 5, 9, 4, 7, 1}
	expected := 306
	actual := score(deck)
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}
