package part2

import (
	"fmt"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/utilities"
)

type Part2 struct{}

type (
	Player int
	Game   struct {
		number                 int
		Player1, Player2       Deck
		previousConfigurations []map[Player]Deck
	}
	Deck []int
)

const (
	Player1 = iota
	Player2
)

var gameCount int

func init() {
	gameCount = 0
}

func NewGame(player1, player2 Deck) *Game {
	gameCount++
	return &Game{
		number:                 gameCount,
		Player1:                player1.Copy(),
		Player2:                player2.Copy(),
		previousConfigurations: []map[Player]Deck{},
	}
}

func (deck Deck) Copy() Deck {
	newDeck := Deck{}
	for _, val := range deck {
		newDeck = append(newDeck, val)
	}
	return newDeck
}

func (game *Game) Play() (Player, Deck) {
	return playGame(game, 1, game.Player1, game.Player2)
}

func (part Part2) Run(input []string) (answer int, err error) {
	var count int
	for i, line := range input {
		if line == "" {
			count = i
		}
	}
	for input[len(input)-1] == "" {
		input = input[:len(input)-1]
	}
	player1, err := utilities.AsToIs(input[1:count])
	if err != nil {
		return -1, err
	}
	player2, err := utilities.AsToIs(input[count+2:])
	if err != nil {
		return -1, err
	}
	game := NewGame(player1, player2)
	_, finalDeck := game.Play()
	answer = finalDeck.Score()

	return
}

func playGame(game *Game, round int, player1, player2 []int) (Player, Deck) {
	if len(player1) == 0 {
		return Player2, player2
	}
	if len(player2) == 0 {
		return Player1, player1
	}
	//fmt.Printf("Game: %d | Round: %d | Player 1: %v | Player 2: %v\n", game.number, round, player1, player2)
	fmt.Printf("Game: %d | Round: %d\n", game.number, round)
	if game.ContainsPreviousConfigurations(player1, player2) {
		return Player1, player1
	} else {
		game.AddPreviousConfiguration(player1, player2)
	}

	left := player1[0]
	right := player2[0]
	var winner Player
	if len(player1[1:]) >= left && len(player2[1:]) >= right {
		newGame := NewGame(player1[1:left+1], player2[1:right+1])
		winner, _ = newGame.Play()
	} else {
		if left > right {
			winner = Player1
		} else {
			winner = Player2
		}
	}

	if winner == Player1 {
		return playGame(game,
			round+1,
			append(player1[1:], []int{left, right}...),
			player2[1:],
		)
	} else {
		return playGame(game,
			round+1,
			player1[1:],
			append(player2[1:], []int{right, left}...),
		)
	}
}

func (game Game) ContainsPreviousConfigurations(player1, player2 Deck) bool {
	for _, config := range game.previousConfigurations {
		if config[Player1].Equals(player1) && config[Player2].Equals(player2) {
			return true
		}
	}

	return false
}

func (game *Game) AddPreviousConfiguration(player1, player2 Deck) {
	game.previousConfigurations = append(game.previousConfigurations,
		map[Player]Deck{Player1: player1, Player2: player2})
}

func (deck Deck) Score() (score int) {
	for i, card := range deck {
		score += card * (len(deck) - i)
	}

	return
}

func (deck Deck) Equals(other Deck) bool {
	if len(deck) != len(other) {
		return false
	}
	for i := range deck {
		if other[i] != deck[i] {
			return false
		}
	}
	return true
}
