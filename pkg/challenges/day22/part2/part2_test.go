package part2

import (
	"fmt"
	"testing"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{
		"Player 1:",
		"9",
		"2",
		"6",
		"3",
		"1",
		"",
		"Player 2:",
		"5",
		"8",
		"4",
		"7",
		"10",
	}
	expected := 291
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestGame_Play(t *testing.T) {
	var tests = []struct {
		name           string
		player1        []int
		player2        []int
		expectedDeck   []int
		expectedPlayer Player
	}{
		{
			name:           "Infinite Prevention",
			player1:        []int{43, 19},
			player2:        []int{2, 29, 14},
			expectedDeck:   []int{43, 19},
			expectedPlayer: Player1,
		},
		{
			name:           "Game 1",
			player1:        []int{9, 2, 6, 3, 1},
			player2:        []int{5, 8, 4, 7, 10},
			expectedDeck:   []int{7, 5, 6, 2, 4, 1, 10, 8, 9, 3},
			expectedPlayer: Player2,
		},
		{
			name:           "Game 2",
			player1:        []int{9, 8, 5, 2},
			player2:        []int{10, 1, 7},
			expectedDeck:   []int{5, 10, 2, 9, 8, 7, 1},
			expectedPlayer: Player2,
		},
		{
			name:           "Game 3",
			player1:        []int{8, 1},
			player2:        []int{3, 4, 10, 9, 7, 5},
			expectedDeck:   []int{7, 5, 4, 1, 10, 8, 9, 3},
			expectedPlayer: Player2,
		},
		{
			name:           "Game 4",
			player1:        []int{8},
			player2:        []int{10, 9, 7, 5},
			expectedDeck:   []int{9, 7, 5, 10, 8},
			expectedPlayer: Player2,
		},
		{
			name:           "Game 5",
			player1:        []int{8},
			player2:        []int{10, 9, 7, 5},
			expectedDeck:   []int{9, 7, 5, 10, 8},
			expectedPlayer: Player2,
		},
	}

	for _, test := range tests {
		test := test
		t.Run(fmt.Sprintf("%s--%v---%v", test.name, test.player1, test.player2), func(t *testing.T) {
			game := NewGame(test.player1, test.player2)
			actualPlayer, actualDeck := game.Play()
			t.Run("final deck should match", func(t *testing.T) {
				expected := test.expectedDeck
				actual := actualDeck
				if len(actual) != len(expected) {
					t.Fatalf("lengths did not match, expected: %d, actual: %d", len(expected), len(actual))
				}
				for i := range expected {
					if actual[i] != expected[i] {
						t.Fatalf("differ at index: %d, expected: %v, actual: %v", i, expected, actual)
					}
				}
			})
			t.Run("winning player should be correct", func(t *testing.T) {
				expected := test.expectedPlayer
				actual := actualPlayer
				if actual != expected {
					t.Fatalf("expected: %d, actual: %d", expected, actual)
				}
			})
		})
	}
}

func TestScore(t *testing.T) {
	var tests = []struct {
		deck     Deck
		expected int
	}{
		{
			deck:     Deck{3, 2, 10, 6, 8, 5, 9, 4, 7, 1},
			expected: 306,
		},
		{
			deck:     Deck{7, 5, 6, 2, 4, 1, 10, 8, 9, 3},
			expected: 291,
		},
	}
	for _, test := range tests {
		test := test
		t.Run(fmt.Sprintf("%v", test.deck), func(t *testing.T) {
			expected := test.expected
			actual := test.deck.Score()
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}

func TestGame_ContainsPreviousConfigurations(t *testing.T) {
	deck1 := Deck{9, 2, 6, 3, 1}
	deck2 := Deck{5, 8, 4, 7, 10}
	deck3 := Deck{1, 8, 4, 7, 10}
	game := NewGame(deck1, deck2)
	game.previousConfigurations = []map[Player]Deck{
		{
			Player1: deck1,
			Player2: deck2,
		},
		{
			Player1: deck2,
			Player2: deck3,
		},
	}
	t.Run("when it should contain previous", func(t *testing.T) {
		if !game.ContainsPreviousConfigurations(deck1, deck2) {
			t.Errorf("should have contained (deck1, deck2) but did not")
		}
		if !game.ContainsPreviousConfigurations(deck2, deck3) {
			t.Errorf("should have contained (deck2, deck3) but did not")
		}
	})
	t.Run("when it should not contain previous", func(t *testing.T) {
		if game.ContainsPreviousConfigurations(deck2, deck1) {
			t.Errorf("should not have contained (deck2, deck1) but did")
		}
		if game.ContainsPreviousConfigurations(deck3, deck2) {
			t.Errorf("should not have contained (deck3, deck2) but did")
		}
	})
}
