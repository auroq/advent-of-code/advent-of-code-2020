package part1

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/challenges/day07"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	rules := day07.NewRules()
	for _, line := range input {
		err = rules.AddRule(line)
		if err != nil {
			return
		}
	}

	shinyGold := rules.GetOrAddBag("shiny gold")
	answer = shinyGold.ContainedBySet().Count()
	return
}
