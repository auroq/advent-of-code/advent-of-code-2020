package day07

import (
	"strconv"
	"strings"
)

type Rules map[string]*Bag

type Connection struct {
	count int
	bag   *Bag
}

func NewRules() *Rules {
	return &Rules{}
}

func (rules Rules) GetOrAddBag(bagName string) (bag *Bag) {
	if val, ok := rules[bagName]; !ok {
		bag = NewBag(bagName)
		rules[bagName] = bag
	} else {
		bag = val
	}
	return
}

func (rules Rules) AddRule(text string) error {
	parts := strings.Split(text, " ")
	containerName := strings.Join(parts[:2], " ")

	reqStrings := strings.Split(strings.Join(parts[4:], " "), ", ")
	for _, reqString := range reqStrings {
		if strings.Contains(strings.ToLower(reqString), "no other") {
			continue
		}
		parts := strings.Split(reqString, " ")
		num, err := strconv.Atoi(parts[0])
		if err != nil {
			return err
		}
		containedName := strings.Join(parts[1:3], " ")
		rules.addConnection(num, containerName, containedName)
	}

	return nil
}

func (rules Rules) addConnection(count int, containerName, containedName string) {
	container := rules.GetOrAddBag(containerName)
	contained := rules.GetOrAddBag(containedName)
	contained.containedBy = append(contained.containedBy, Connection{
		count: count,
		bag:   container,
	})
	container.mustContain = append(container.mustContain, Connection{
		count: count,
		bag:   contained,
	})
}
