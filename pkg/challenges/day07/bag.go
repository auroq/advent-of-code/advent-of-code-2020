package day07

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/utilities/datastructures"
)

type Bag struct {
	name        string
	containedBy []Connection
	mustContain []Connection
}

func NewBag(name string) *Bag {
	return &Bag{name: name, containedBy: []Connection{}}
}

func (bag *Bag) ContainedBySet() (containers *datastructures.Set) {
	var containedByHelper func(set *datastructures.Set, bag *Bag) *datastructures.Set
	containedByHelper = func(set *datastructures.Set, bag *Bag) *datastructures.Set {
		for _, container := range bag.containedBy {
			set.Add(container.bag.name)
			containedByHelper(set, container.bag)
		}
		return set
	}
	return containedByHelper(datastructures.NewSet(), bag)
}

func (bag *Bag) MustContainList() (bags []string) {
	var mustContainHelper func(bags []string, bag *Bag) []string
	mustContainHelper = func(bags []string, bag *Bag) []string {
		for _, contained := range bag.mustContain {
			for i := 0; i < contained.count; i++ {
				bags = append(bags, contained.bag.name)
				bags = mustContainHelper(bags, contained.bag)
			}
		}
		return bags
	}
	return mustContainHelper([]string{}, bag)
}
