package part2

import (
	"strings"
	"testing"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{
		"1 + 2 * 3 + 4 * 5 + 6",
		"1 + (2 * 3) + (4 * (5 + 6))",
		"2 * 3 + (4 * 5)",
		"5 + (8 * 3 + 9 + 3 * 4 * 3)",
		"5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))",
		"((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2",
	}
	expected := 694173
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestInfixToPostFix(t *testing.T) {
	var tests = []struct {
		problem  string
		expected string
	}{
		{
			"1 + 2 * 3 + 4 * 5 + 6",
			"1 2 + 3 4 + * 5 6 + *",
		},
		{
			"1 + (2 * 3) + (4 * (5 + 6))",
			"1 2 3 * + 4 5 6 + * +",
		},
		{
			"2 * 3 + (4 * 5)",
			"2 3 4 5 * + *",
		},
		{
			"5 + (8 * 3 + 9 + 3 * 4 * 3)",
			"5 8 3 9 + 3 + * 4 * 3 * +",
		},
		{
			"5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))",
			"5 9 * 7 3 * 3 9 + * 3 8 6 + 4 * + * *",
		},
		{
			"((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2",
			"2 4 + 9 * 6 9 + 8 6 + * 6 + * 2 + 4 + 2 *",
		},
	}
	for _, test := range tests {
		test := test
		t.Run(test.problem, func(t *testing.T) {
			t.Parallel()
			expected := test.expected
			actual := strings.Join(InfixToPostFix(test.problem), " ")
			if actual != expected {
				t.Fatalf("expected: %s, actual: %s", expected, actual)
			}
		})
	}
}

func TestEvaluatePostFix(t *testing.T) {
	var tests = []struct {
		problem  string
		expected int
	}{
		{
			"1 2 + 3 4 + * 5 6 + *",
			231,
		},
		{
			"1 2 3 * + 4 5 6 + * +",
			51,
		},
		{
			"2 3 4 5 * + *",
			46,
		},
		{
			"5 8 3 9 + 3 + * 4 * 3 * +",
			1445,
		},
		{
			"5 9 * 7 3 * 3 9 + * 3 8 6 + 4 * + * *",
			669060,
		},
		{
			"2 4 + 9 * 6 9 + 8 6 + * 6 + * 2 + 4 + 2 *",
			23340,
		},
	}
	for _, test := range tests {
		test := test
		t.Run(test.problem, func(t *testing.T) {
			t.Parallel()
			expected := test.expected
			actual := EvaluatePostfix(strings.Split(test.problem, " "))
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}
