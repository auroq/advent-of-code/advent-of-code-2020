package part1

import (
	"strings"
	"testing"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"1 + 2 * 3 + 4 * 5 + 6",
		"1 + (2 * 3) + (4 * (5 + 6))",
		"2 * 3 + (4 * 5)",
		"5 + (8 * 3 + 9 + 3 * 4 * 3)",
		"5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))",
		"((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2",
	}
	expected := 26457
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestInfixToPostFix(t *testing.T) {
	var tests = []struct {
		problem  string
		expected string
	}{
		{
			"1 + 2 * 3 + 4 * 5 + 6",
			"1 2 + 3 * 4 + 5 * 6 +",
		},
		{
			"1 + (2 * 3) + (4 * (5 + 6))",
			"1 2 3 * + 4 5 6 + * +",
		},
		{
			"2 * 3 + (4 * 5)",
			"2 3 * 4 5 * +",
		},
		{
			"5 + (8 * 3 + 9 + 3 * 4 * 3)",
			"5 8 3 * 9 + 3 + 4 * 3 * +",
		},
		{
			"5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))",
			"5 9 * 7 3 * 3 * 9 + 3 * 8 6 + 4 * + *",
		},
		{
			"((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2",
			"2 4 + 9 * 6 9 + 8 * 6 + * 6 + 2 + 4 + 2 *",
		},
	}
	for _, test := range tests {
		test := test
		t.Run(test.problem, func(t *testing.T) {
			t.Parallel()
			expected := test.expected
			actual := strings.Join(InfixToPostFix(test.problem), " ")
			if actual != expected {
				t.Fatalf("expected: %s, actual: %s", expected, actual)
			}
		})
	}
}

func TestEvaluatePostFix(t *testing.T) {
	var tests = []struct {
		problem  string
		expected int
	}{
		{
			"1 2 + 3 * 4 + 5 * 6 +",
			71,
		},
		{
			"1 2 3 * + 4 5 6 + * +",
			51,
		},
		{
			"2 3 * 4 5 * +",
			26,
		},
		{
			"5 8 3 * 9 + 3 + 4 * 3 * +",
			437,
		},
		{
			"5 9 * 7 3 * 3 * 9 + 3 * 8 6 + 4 * + *",
			12240,
		},
		{
			"2 4 + 9 * 6 9 + 8 * 6 + * 6 + 2 + 4 + 2 *",
			13632,
		},
	}
	for _, test := range tests {
		test := test
		t.Run(test.problem, func(t *testing.T) {
			t.Parallel()
			expected := test.expected
			actual := EvaluatePostfix(strings.Split(test.problem, " "))
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}
