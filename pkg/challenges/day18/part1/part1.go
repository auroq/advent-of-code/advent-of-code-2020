package part1

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/utilities/datastructures"
	"strconv"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	for _, line := range input {
		postFix := InfixToPostFix(line)
		answer += EvaluatePostfix(postFix)
	}
	return
}

var operatorPrecedence = map[string]int{
	"+": 1,
	"-": 1,
	"/": 1,
	"*": 1,
}

func EvaluatePostfix(problem []string) int {
	stack := datastructures.NewStack()
	for _, item := range problem {
		if num, err := strconv.Atoi(item); err == nil {
			stack.Push(num)
		} else {
			right := stack.Pop().(int)
			left := stack.Pop().(int)
			stack.Push(operate(left, right, item))
		}
	}

	return stack.Top().(int)
}

func operate(left, right int, operator string) int {
	return map[string]func(left, right int) int{
		"+": func(left, right int) int { return left + right },
		"-": func(left, right int) int { return left - right },
		"*": func(left, right int) int { return left * right },
		"/": func(left, right int) int { return left / right },
	}[operator](left, right)
}

func InfixToPostFix(expression string) (output []string) {
	stack := datastructures.NewStack()
	for _, char := range expression {
		if item := string(char); item == " " {
			continue
		} else if item == "(" {
			stack.Push(item)
		} else if _, err := strconv.Atoi(item); err == nil {
			output = append(output, item)
		} else if item == "+" || item == "-" || item == "*" || item == "/" {
			for stack.Size() >= 1 &&
				stack.Top() != "(" &&
				operatorPrecedence[stack.Top().(string)] >= operatorPrecedence[item] {
				output = append(output, stack.Pop().(string))
			}
			stack.Push(item)
		} else {
			output = append(output, stack.Pop().(string))
			for stack.Top() != "(" {
				output = append(output, stack.Pop().(string))
			}
			stack.Pop()
		}
	}
	for stack.Size() > 0 {
		output = append(output, stack.Pop().(string))
	}

	return
}
