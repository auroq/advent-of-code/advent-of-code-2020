package part2

import (
	"fmt"
	"sort"
	"testing"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{
		"28",
		"33",
		"18",
		"42",
		"31",
		"14",
		"46",
		"20",
		"48",
		"47",
		"24",
		"23",
		"49",
		"45",
		"19",
		"38",
		"39",
		"11",
		"1",
		"32",
		"25",
		"35",
		"8",
		"17",
		"7",
		"9",
		"4",
		"2",
		"34",
		"10",
		"3",
	}
	expected := 19208
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestCountPermutations(t *testing.T) {
	var tests = []struct {
		nums     []int
		expected int
	}{
		{[]int{16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4}, 8},
		{[]int{28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3}, 19208},
	}
	for _, test := range tests {
		t.Run(fmt.Sprintf("%v", test.nums), func(t *testing.T) {
			nums := test.nums
			sort.Ints(nums)
			expected := test.expected
			actual := countPermutations(nums)
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}
