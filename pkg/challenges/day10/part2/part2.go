package part2

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/utilities"
	"sort"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	nums, err := utilities.AsToIs(input)
	if err != nil {
		return -1, err
	}
	sort.Ints(nums)
	answer = countPermutations(nums)
	return
}

func countPermutations(nums []int) int {
	nums = append(nums, nums[len(nums)-1]+3)
	set := map[int]int{}
	set[0] = 1
	for _, val := range nums {
		set[val] = set[val-1] + set[val-2] + set[val-3]
	}
	return set[nums[len(nums)-1]]
}
