package part1

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/utilities"
	"sort"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	nums, err := utilities.AsToIs(input)
	if err != nil {
		return -1, err
	}
	sort.Ints(nums)
	ones, threes := count(nums)
	answer = ones * threes
	return
}

func count(input []int) (ones, threes int) {
	ones = 1   // always start at 0
	threes = 1 // your device's built-in adapter is always 3 higher than the highest adapter
	for i, val := range input {
		if i == 0 {
			continue
		}
		if val-input[i-1] == 3 {
			threes++
		}
		if val-input[i-1] == 1 {
			ones++
		}
	}

	return
}
