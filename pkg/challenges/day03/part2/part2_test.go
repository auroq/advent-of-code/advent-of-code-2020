package part2

import (
	"strconv"
	"testing"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{
		"..##.......",
		"#...#...#..",
		".#....#..#.",
		"..#.#...#.#",
		".#...##..#.",
		"..#.##.....",
		".#.#.#....#",
		".#........#",
		"#.##...#...",
		"#...##....#",
		".#..#...#.#",
	}
	expected := 336
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestCountTrees(t *testing.T) {
	var input = []string{
		"..##.......",
		"#...#...#..",
		".#....#..#.",
		"..#.#...#.#",
		".#...##..#.",
		"..#.##.....",
		".#.#.#....#",
		".#........#",
		"#.##...#...",
		"#...##....#",
		".#..#...#.#",
	}
	var tests = []struct {
		slope  slope
		answer int
	}{
		{slope{1, 1}, 2},
		{slope{3, 1}, 7},
		{slope{5, 1}, 3},
		{slope{7, 1}, 4},
		{slope{1, 2}, 2},
	}
	for i, test := range tests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			expected := test.answer
			actual := test.slope.CountTrees(input)
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}

func TestIsHit(t *testing.T) {
	var tests = []struct {
		row string
		pos int
		hit bool
	}{
		{"#...#...#..", 3, false},
		{".#....#..#.", 6, true},
		{"..#.#...#.#", 9, false},
		{".#...##..#.", 12, true},
		{"..#.##.....", 15, true},
		{".#.#.#....#", 18, false},
		{".#........#", 21, true},
		{"#.##...#...", 24, true},
		{"#...##....#", 27, true},
		{".#..#...#.#", 30, true},
	}
	for i, test := range tests {
		actual := isHit(test.row, test.pos)
		expected := test.hit
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			if actual != expected {
				t.Fatalf("expected: %t, actual: %t", expected, actual)
			}
		})
	}
}
