package part2

type Part2 struct{}

type slope struct {
	x int
	y int
}

func (day Part2) Run(input []string) (answer int, err error) {
	var slopes = []slope{
		{1, 1},
		{3, 1},
		{5, 1},
		{7, 1},
		{1, 2},
	}
	answer = 1
	for _, slope := range slopes {
		answer *= slope.CountTrees(input)
	}

	return
}

func (slope slope) CountTrees(input []string) (treesHit int) {
	pos := slope.x
	for i := slope.y; i < len(input); i += slope.y {
		if isHit(input[i], pos) {
			treesHit++
		}
		pos += slope.x
	}

	return
}

func isHit(row string, pos int) bool {
	for len(row) <= pos {
		row += row
	}
	return rune(row[pos]) == '#'
}
