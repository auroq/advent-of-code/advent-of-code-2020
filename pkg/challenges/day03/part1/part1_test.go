package part1

import (
	"strconv"
	"testing"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"..##.......",
		"#...#...#..",
		".#....#..#.",
		"..#.#...#.#",
		".#...##..#.",
		"..#.##.....",
		".#.#.#....#",
		".#........#",
		"#.##...#...",
		"#...##....#",
		".#..#...#.#",
	}
	expected := 7
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestIsHit(t *testing.T) {
	var tests = []struct {
		row string
		pos int
		hit bool
	}{
		{"#...#...#..", 3, false},
		{".#....#..#.", 6, true},
		{"..#.#...#.#", 9, false},
		{".#...##..#.", 12, true},
		{"..#.##.....", 15, true},
		{".#.#.#....#", 18, false},
		{".#........#", 21, true},
		{"#.##...#...", 24, true},
		{"#...##....#", 27, true},
		{".#..#...#.#", 30, true},
	}
	for i, test := range tests {
		actual := isHit(test.row, test.pos)
		expected := test.hit
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			if actual != expected {
				t.Fatalf("expected: %t, actual: %t", expected, actual)
			}
		})
	}
}
