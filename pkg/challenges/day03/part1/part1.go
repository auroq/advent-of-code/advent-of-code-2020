package part1

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	pos := 3
	for i, line := range input {
		if i == 0 {
			continue
		}
		if isHit(line, pos) {
			answer++
		}
		pos += 3
	}

	return
}

func isHit(row string, pos int) bool {
	for len(row) < pos {
		row += row
	}
	return rune(row[pos]) == '#'
}
