package part2

import (
	"fmt"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/challenges/day23/datastructure"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/utilities"
	"strings"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	nums, err := utilities.AsToIs(strings.Split(input[0], ""))
	if err != nil {
		return -1, err
	}
	cups := datastructure.NewCups()
	for _, num := range nums {
		_, err = cups.Add(num)
		if err != nil {
			return
		}
	}
	for i := len(nums) + 1; i <= 1_000_000; i++ {
		_, err = cups.Add(i)
		if err != nil {
			return
		}
	}
	for i := 0; i < 10_000_000; i++ {
		if i%500 == 0 {
			fmt.Printf("-- move %d --\n", i+1)
		}
		cups.Iterate()
	}

	//fmt.Println("-- final --")
	//cups.Print()

	one, two := cups.TwoAfterOne()
	answer = one * two
	return
}
