package datastructure

import (
	"fmt"
	"math"
	"strconv"
)

type (
	Cups struct {
		cursor *Cup
		cups   map[int]*Cup
		min    int
		max    int
	}

	Cup struct {
		value int
		left  *Cup
		right *Cup
	}
)

func NewCups() *Cups {
	return &Cups{
		cursor: nil,
		cups:   map[int]*Cup{},
		min:    math.MaxInt32,
		max:    math.MinInt32,
	}
}

func NewCup(val int, left, right *Cup) *Cup {
	return &Cup{
		value: val,
		left:  left,
		right: right,
	}
}

func (cups *Cups) Add(val int) (newCup *Cup, err error) {
	if _, ok := cups.cups[val]; ok {
		err = fmt.Errorf("cup with value '%d' already exists", val)
		return
	}
	if cups.cursor == nil {
		newCup = NewCup(val, nil, nil)
		newCup.left = newCup
		newCup.right = newCup
		cups.cursor = newCup
	} else {
		newCup = NewCup(val, cups.cursor.left, cups.cursor)
		cups.cursor.left.right = newCup
		cups.cursor.left = newCup
	}
	cups.cups[val] = newCup
	if val > cups.max {
		cups.max = val
	}
	if val < cups.min {
		cups.min = val
	}

	return
}

func (cups *Cups) Next() *Cup {
	cups.cursor = cups.cursor.right
	return cups.cursor
}

func (cups Cups) Assemble() (int, error) {
	current := cups.cups[1].right
	str := ""
	for current.value != 1 {
		str += strconv.Itoa(current.value)
		current = current.right
	}

	return strconv.Atoi(str)
}

func (cups *Cups) Iterate() {
	pickedUp1 := cups.cursor.right
	pickedUp2 := pickedUp1.right
	pickedUp3 := pickedUp2.right
	delete(cups.cups, pickedUp1.value)
	delete(cups.cups, pickedUp2.value)
	delete(cups.cups, pickedUp3.value)
	cups.cursor.right = pickedUp3.right
	pickedUp3.right.left = cups.cursor

	destination := cups.cursor.value
	var node *Cup = nil
	found := false
	for !found {
		destination--
		if destination < cups.min {
			destination = cups.max
		}
		node, found = cups.cups[destination]
	}
	pickedUp3.right = node.right
	node.right.left = pickedUp3

	node.right = pickedUp1
	pickedUp1.left = node

	cups.cups[pickedUp1.value] = pickedUp1
	cups.cups[pickedUp2.value] = pickedUp2
	cups.cups[pickedUp3.value] = pickedUp3

	cups.Next()
}

func (cups Cups) Print() {
	fmt.Printf("%d", cups.cursor.value)
	current := cups.cursor.right
	for current.value != cups.cursor.value {
		fmt.Printf(", %d", current.value)
		current = current.right
	}
	fmt.Println()
}

func (cups Cups) TwoAfterOne() (int, int) {
	current := cups.cups[1]
	return current.right.value, current.right.right.value
}
