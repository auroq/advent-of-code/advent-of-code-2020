package part1

import (
	"testing"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{"389125467"}
	expected := 67384529
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}
