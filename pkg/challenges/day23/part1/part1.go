package part1

import (
	"fmt"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/challenges/day23/datastructure"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/utilities"
	"strings"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	nums, err := utilities.AsToIs(strings.Split(input[0], ""))
	if err != nil {
		return -1, err
	}

	cups := datastructure.NewCups()
	for _, num := range nums {
		_, err = cups.Add(num)
		if err != nil {
			return
		}
	}

	for i := 0; i < 100; i++ {
		fmt.Printf("-- move %d --\n", i+1)
		//cups.Print()
		cups.Iterate()
	}

	return cups.Assemble()
}
