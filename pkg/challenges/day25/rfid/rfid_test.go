package rfid_test

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/challenges/day25/rfid"
	"testing"
)

func TestEncrypt(t *testing.T) {
	var tests = []struct {
		name          string
		subjectNumber int
		loopSize      int
		expected      int
	}{
		{
			name:          "card public key",
			subjectNumber: 7,
			loopSize:      8,
			expected:      5764801,
		},
		{
			name:          "door public key",
			subjectNumber: 7,
			loopSize:      11,
			expected:      17807724,
		},
		{
			name:          "encryption key from card encrypting door public key",
			subjectNumber: 17807724,
			loopSize:      8,
			expected:      14897079,
		},
		{
			name:          "encryption key from door encrypting card public key",
			subjectNumber: 5764801,
			loopSize:      11,
			expected:      14897079,
		},
	}
	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			expected := test.expected
			actual := rfid.Encrypt(test.subjectNumber, test.loopSize)
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}

func TestGetLoopSize(t *testing.T) {
	subjectNumber := 7
	publicKey := 5764801
	expected := 8
	actual := rfid.GetLoopSize(subjectNumber, publicKey)
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}
