package rfid

func Encrypt(subjectNumber, loopSize int) (value int) {
	divisor := 20201227
	value = 1
	for i := 0; i < loopSize; i++ {
		value = (value * subjectNumber) % divisor
	}
	return
}

func GetLoopSize(subjectNumber, publicKey int) (loopSize int) {
	divisor := 20201227
	value := 1
	loopSize = 0
	for value != publicKey {
		loopSize++
		value = (value * subjectNumber) % divisor
	}

	return
}
