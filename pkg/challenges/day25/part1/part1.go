package part1

import (
	"fmt"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/challenges/day25/rfid"
	"strconv"
)

type Part1 struct{}

const subjectNumber = 7

func (part Part1) Run(input []string) (answer int, err error) {
	var cardPublicKey, doorPublicKey int
	cardPublicKey, err = strconv.Atoi(input[0])
	if err != nil {
		return
	}
	doorPublicKey, err = strconv.Atoi(input[1])
	if err != nil {
		return
	}

	doorLoopSize := rfid.GetLoopSize(subjectNumber, doorPublicKey)
	cardLoopSize := rfid.GetLoopSize(subjectNumber, cardPublicKey)

	doorEncryption := rfid.Encrypt(cardPublicKey, doorLoopSize)
	cardEncryption := rfid.Encrypt(doorPublicKey, cardLoopSize)

	answer = doorEncryption
	if doorEncryption != cardEncryption {
		err = fmt.Errorf("encryption keys didn't match! card: %d, door: %d", cardEncryption, doorEncryption)
	}

	return
}
