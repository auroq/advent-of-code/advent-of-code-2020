package part1

import (
	"strconv"
	"strings"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	mem := NewMemory()
	for _, line := range input {
		if line[:4] == "mask" {
			mem.SetMask(strings.Split(line, " ")[2])
		} else {
			parts := strings.Split(line, " ")
			val, err := strconv.Atoi(parts[2])
			if err != nil {
				return -1, err
			}
			address, err := strconv.Atoi(parts[0][4 : len(parts[0])-1])
			if err != nil {
				return -1, err
			}
			err = mem.SetAddress(address, int64(val))
			if err != nil {
				return -1, err
			}
		}
	}
	answer = int(mem.Sum())
	return
}

type Memory struct {
	mask      BitMask
	addresses map[int]int64
}

func NewMemory() *Memory {
	return &Memory{
		mask:      "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
		addresses: map[int]int64{},
	}
}

func (memory *Memory) SetMask(bitMask string) {
	memory.mask = BitMask(bitMask)
}

func (memory Memory) Sum() (sum int64) {
	for _, address := range memory.addresses {
		sum += address
	}
	return
}

func (memory *Memory) SetAddress(address int, value int64) error {
	val, err := memory.mask.Mask(value)
	if err != nil {
		return err
	}
	memory.addresses[address] = val
	return nil
}

type BitMask string

func (mask BitMask) Mask(value int64) (int64, error) {
	op1 := strings.ReplaceAll(string(mask), "X", "0")
	op2 := strings.ReplaceAll(string(mask), "X", "1")
	maskOr, err := strconv.ParseInt(op1, 2, 64)
	if err != nil {
		return 0, err
	}
	maskAnd, err := strconv.ParseInt(op2, 2, 64)
	if err != nil {
		return 0, err
	}
	return (value | maskOr) & maskAnd, nil
}
