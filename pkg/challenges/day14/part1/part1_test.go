package part1

import (
	"strconv"
	"testing"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X",
		"mem[8] = 11",
		"mem[7] = 101",
		"mem[8] = 0",
	}
	expected := 165
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestMask(t *testing.T) {
	var tests = []struct {
		mask     BitMask
		value    int64
		expected int64
	}{
		{"XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X", 11, 0b1001001},
		{"XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X", 101, 0b1100101},
		{"XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X", 0, 0b1000000},
	}
	for _, test := range tests {
		t.Run(strconv.Itoa(int(test.value)), func(t *testing.T) {
			expected := test.expected
			actual, err := test.mask.Mask(test.value)
			if err != nil {
				t.Error(err)
			}
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}

}
