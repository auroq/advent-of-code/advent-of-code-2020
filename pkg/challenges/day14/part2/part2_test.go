package part2

import (
	"strconv"
	"testing"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{
		"mask = 000000000000000000000000000000X1001X",
		"mem[42] = 100",
		"mask = 00000000000000000000000000000000X0XX",
		"mem[26] = 1",
	}
	expected := 208
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestMask(t *testing.T) {
	var tests = []struct {
		mask              BitMask
		address           int64
		expectedAddresses []int64
	}{
		{"000000000000000000000000000000X1001X", 42, []int64{26, 27, 58, 59}},
		{"00000000000000000000000000000000X0XX", 26, []int64{16, 17, 18, 19, 24, 25, 26, 27}},
	}
	for _, test := range tests {
		t.Run(strconv.Itoa(int(test.address)), func(t *testing.T) {
			expectedAddresses := test.expectedAddresses
			actualAddresses, err := test.mask.Mask(test.address)
			if err != nil {
				t.Error(err)
			}

			for i := 0; i < len(expectedAddresses); i++ {
				actual := actualAddresses[i]
				expected := expectedAddresses[i]
				if actual != expected {
					t.Fatalf("expected: %d, actual: %d", expected, actual)
				}
			}
		})
	}

}
