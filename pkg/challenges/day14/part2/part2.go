package part2

import (
	"fmt"
	"strconv"
	"strings"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	mem := NewMemory()
	for _, line := range input {
		if line[:4] == "mask" {
			mem.SetMask(strings.Split(line, " ")[2])
		} else {
			parts := strings.Split(line, " ")
			val, err := strconv.Atoi(parts[2])
			if err != nil {
				return -1, err
			}
			address, err := strconv.Atoi(parts[0][4 : len(parts[0])-1])
			if err != nil {
				return -1, err
			}
			err = mem.WriteAddress(int64(address), int64(val))
			if err != nil {
				return -1, err
			}
		}
	}
	answer = int(mem.Sum())
	return
}

type Memory struct {
	mask      BitMask
	addresses map[int64]int64
}

func NewMemory() *Memory {
	return &Memory{
		mask:      "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
		addresses: map[int64]int64{},
	}
}

func (memory *Memory) SetMask(bitMask string) {
	memory.mask = BitMask(bitMask)
}

func (memory Memory) Sum() (sum int64) {
	for _, address := range memory.addresses {
		sum += address
	}
	return
}

func (memory *Memory) WriteAddress(address int64, value int64) error {
	maskedAddresses, err := memory.mask.Mask(address)
	if err != nil {
		return err
	}
	for _, maskedAddress := range maskedAddresses {
		memory.addresses[maskedAddress] = value
	}
	return nil
}

type BitMask string

func (mask BitMask) Mask(value int64) (maskedAddresses []int64, err error) {
	op1 := strings.ReplaceAll(string(mask), "X", "0")
	maskOr, err := strconv.ParseInt(op1, 2, 64)
	if err != nil {
		return nil, err
	}
	initialMasked := value | maskOr
	str := fmt.Sprintf("%036b", initialMasked)
	for i, r := range mask {
		if r == 'X' {
			str = str[:i] + "X" + str[i+1:]
		}
	}
	for _, permutation := range permutate(str) {
		maskedAddress, err := strconv.ParseInt(permutation, 2, 64)
		if err != nil {
			return nil, err
		}
		maskedAddresses = append(maskedAddresses, maskedAddress)
	}
	return
}

func permutate(val string) (permutations []string) {
	if index := strings.IndexRune(val, 'X'); index >= 0 {
		permutations = append(permutations, permutate(val[0:index]+"0"+val[index+1:])...)
		permutations = append(permutations, permutate(val[0:index]+"1"+val[index+1:])...)
	} else {
		permutations = append(permutations, val)
	}

	return permutations
}
