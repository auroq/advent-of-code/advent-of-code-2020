package part1

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/challenges/day17/part1/pocket"
	"testing"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		".#.",
		"..#",
		"###",
	}
	expected := 112
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestGetOccupied(t *testing.T) {
	lines := []string{
		"#.##.##.##",
		"#######.##",
	}
	layout := pocket.NewGrid(lines)
	actual := GetOccupied(layout, pocket.Position{X: 2, Y: 0, Z: 0})
	expected := 4
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestIterateCell(t *testing.T) {
	startingCells := map[pocket.Position]pocket.Cell{
		pocket.Position{X: 1, Y: 0, Z: 0}: pocket.ActiveCell(),
		pocket.Position{X: 2, Y: 1, Z: 0}: pocket.ActiveCell(),
		pocket.Position{X: 0, Y: 2, Z: 0}: pocket.ActiveCell(),
		pocket.Position{X: 1, Y: 2, Z: 0}: pocket.ActiveCell(),
		pocket.Position{X: 2, Y: 2, Z: 0}: pocket.ActiveCell(),
	}
	expectedActiveCells := map[pocket.Position]pocket.Cell{
		pocket.Position{X: 0, Y: 1, Z: -1}: pocket.ActiveCell(),
		pocket.Position{X: 0, Y: 1, Z: -1}: pocket.ActiveCell(),
		pocket.Position{X: 1, Y: 2, Z: -1}: pocket.ActiveCell(),
		pocket.Position{X: 0, Y: 1, Z: 0}:  pocket.ActiveCell(),
		pocket.Position{X: 2, Y: 1, Z: 0}:  pocket.ActiveCell(),
		pocket.Position{X: 1, Y: 2, Z: 0}:  pocket.ActiveCell(),
		pocket.Position{X: 2, Y: 2, Z: 0}:  pocket.ActiveCell(),
		pocket.Position{X: 0, Y: 1, Z: 1}:  pocket.ActiveCell(),
		pocket.Position{X: 1, Y: 2, Z: 1}:  pocket.ActiveCell(),
	}
	for z := -1; z <= 1; z++ {
		for y := -1; y <= 1; y++ {
			for x := -1; x <= 1; x++ {
				position := pocket.Position{X: x, Y: y, Z: z}
				t.Run(position.String(), func(t *testing.T) {
					t.Parallel()
					grid := pocket.EmptyGrid()
					grid.Cells = startingCells
					expected := pocket.InactiveCell()
					if cell, ok := expectedActiveCells[position]; ok {
						expected = cell
					}
					startingCell := pocket.InactiveCell()
					if cell, ok := grid.Cells[position]; ok {
						startingCell = cell
					}
					actual := iterateCell(startingCell, grid, position)
					if expected != actual {
						t.Errorf("position: %s, expected: %s, actual: %s", position, expected, actual)
					}
				})
			}
		}
	}
}
