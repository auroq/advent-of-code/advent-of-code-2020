package pocket

const (
	active   = "#"
	inactive = "."
)

type Cell string

func ActiveCell() Cell   { return active }
func InactiveCell() Cell { return inactive }

func NewCell(val rune) Cell {
	return map[rune]Cell{
		'#': active,
		'.': inactive,
	}[val]
}

func (cell Cell) String() string {
	return map[Cell]string{
		active:   "#",
		inactive: ".",
	}[cell]
}

func (cell Cell) IsActive() bool { return cell == active }

func (cell Cell) Invert() Cell {
	switch cell {
	case active:
		return inactive
	default:
		return active
	}
}
