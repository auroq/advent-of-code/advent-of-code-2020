package pocket

import (
	"fmt"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/utilities"
	"math"
)

type (
	Grid struct {
		dimensions *Dimensions
		Cells      map[Position]Cell
	}

	Dimensions struct {
		Width, Height, Depth Dimension
	}

	Dimension struct {
		Max, Min int
	}

	Position struct {
		X, Y, Z int
	}
)

func (grid Grid) Depth() Dimension  { return grid.Dimensions().Depth }
func (grid Grid) Height() Dimension { return grid.Dimensions().Height }
func (grid Grid) Width() Dimension  { return grid.Dimensions().Width }

func EmptyGrid() Grid {
	return Grid{
		Cells: map[Position]Cell{},
	}
}

func NewGrid(input []string) Grid {
	layout := Grid{
		Cells: map[Position]Cell{},
	}
	z := 0
	for rowIndex, row := range input {
		for colIndex, col := range row {
			if cell := NewCell(col); cell.IsActive() {
				layout.Cells[Position{X: colIndex, Y: rowIndex, Z: z}] = cell
			}
		}
	}

	return layout
}

func (grid Grid) Copy() Grid {
	copyLayout := Grid{}
	for pos, cell := range grid.Cells {
		copyLayout.Cells[pos] = cell
	}

	return copyLayout
}

func (grid Grid) Dimensions() Dimensions {
	if grid.dimensions != nil {
		return *grid.dimensions
	}
	minX, minY, minZ := grid.Min()
	maxX, maxY, maxZ := grid.Max()
	return Dimensions{
		Width:  Dimension{Min: minX, Max: maxX},
		Height: Dimension{Min: minY, Max: maxY},
		Depth:  Dimension{Min: minZ, Max: maxZ},
	}
}

func (grid Grid) Min() (x, y, z int) {
	x = math.MaxInt32
	y = math.MaxInt32
	z = math.MaxInt32
	for position, _ := range grid.Cells {
		x = utilities.Min(x, position.X)
		y = utilities.Min(y, position.Y)
		z = utilities.Min(z, position.Z)
	}
	return
}

func (grid Grid) Max() (x, y, z int) {
	x = math.MinInt32
	y = math.MinInt32
	z = math.MinInt32
	for position, _ := range grid.Cells {
		x = utilities.Max(x, position.X)
		y = utilities.Max(y, position.Y)
		z = utilities.Max(z, position.Z)
	}
	return
}

func (grid Grid) String() (str string) {
	for depth := grid.Depth().Min; depth <= grid.Depth().Max; depth++ {
		str += fmt.Sprintf("z=%d\n", depth)
		for row := grid.Height().Min; row <= grid.Height().Max; row++ {
			for col := grid.Width().Min; col <= grid.Width().Max; col++ {
				if cell, ok := grid.Cells[Position{X: col, Y: row, Z: depth}]; ok {
					str += cell.String()
				} else {
					str += "."
				}
			}
			str += "\n"
		}
		str += "\n"
	}
	return
}

func (grid Grid) Equals(other Grid) bool {
	for position, cell := range grid.Cells {
		if otherCell, ok := other.Cells[position]; !ok || otherCell != cell {
			return false
		}
	}

	return true
}

func (grid Grid) CountOccupied() (count int) {
	for _, cell := range grid.Cells {
		if cell == active {
			count++
		}
	}

	return
}

func (position Position) String() string {
	return fmt.Sprintf("[%d, %d, %d]", position.X, position.Y, position.Z)
}

func (grid Grid) Iterate(cellIteration func(cell Cell, grid Grid, position Position) Cell) Grid {
	nextGrid := EmptyGrid()
	for depth := grid.Depth().Min - 1; depth <= grid.Depth().Max+1; depth++ {
		for row := grid.Height().Min - 1; row <= grid.Height().Max+1; row++ {
			for col := grid.Width().Min - 1; col <= grid.Width().Max+1; col++ {
				var newCell Cell
				position := Position{X: col, Y: row, Z: depth}
				if cell, ok := grid.Cells[position]; ok {
					newCell = cellIteration(cell, grid, position)
				} else {
					newCell = cellIteration(InactiveCell(), grid, position)
				}
				if newCell.IsActive() {
					nextGrid.Cells[position] = newCell
				}
			}
		}
	}

	return nextGrid
}
