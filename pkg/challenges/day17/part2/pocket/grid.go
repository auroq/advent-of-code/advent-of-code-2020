package pocket

import (
	"fmt"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/utilities"
	"math"
)

type (
	Grid struct {
		dimensions *Dimensions
		Cells      map[Position]Cell
	}

	Dimensions struct {
		X, Y, Z, W Dimension
	}

	Dimension struct {
		Max, Min int
	}

	Position struct {
		X, Y, Z, W int
	}
)

func (grid Grid) W() Dimension { return grid.Dimensions().W }
func (grid Grid) Z() Dimension { return grid.Dimensions().Z }
func (grid Grid) Y() Dimension { return grid.Dimensions().Y }
func (grid Grid) X() Dimension { return grid.Dimensions().X }

func EmptyGrid() Grid {
	return Grid{
		Cells: map[Position]Cell{},
	}
}

func NewGrid(input []string) Grid {
	layout := Grid{
		Cells: map[Position]Cell{},
	}
	z := 0
	w := 0
	for rowIndex, row := range input {
		for colIndex, col := range row {
			if cell := NewCell(col); cell.IsActive() {
				layout.Cells[Position{X: colIndex, Y: rowIndex, Z: z, W: w}] = cell
			}
		}
	}

	return layout
}

func (grid Grid) Copy() Grid {
	copyLayout := Grid{}
	for pos, cell := range grid.Cells {
		copyLayout.Cells[pos] = cell
	}

	return copyLayout
}

func (grid Grid) Dimensions() Dimensions {
	if grid.dimensions != nil {
		return *grid.dimensions
	}
	minX, minY, minZ, minW := grid.Min()
	maxX, maxY, maxZ, maxW := grid.Max()
	return Dimensions{
		X: Dimension{Min: minX, Max: maxX},
		Y: Dimension{Min: minY, Max: maxY},
		Z: Dimension{Min: minZ, Max: maxZ},
		W: Dimension{Min: minW, Max: maxW},
	}
}

func (grid Grid) Min() (x, y, z, w int) {
	x = math.MaxInt32
	y = math.MaxInt32
	z = math.MaxInt32
	w = math.MaxInt32
	for position, _ := range grid.Cells {
		x = utilities.Min(x, position.X)
		y = utilities.Min(y, position.Y)
		z = utilities.Min(z, position.Z)
		w = utilities.Min(w, position.W)
	}
	return
}

func (grid Grid) Max() (x, y, z, w int) {
	x = math.MinInt32
	y = math.MinInt32
	z = math.MinInt32
	w = math.MinInt32
	for position, _ := range grid.Cells {
		x = utilities.Max(x, position.X)
		y = utilities.Max(y, position.Y)
		z = utilities.Max(z, position.Z)
		w = utilities.Max(w, position.W)
	}
	return
}

func (grid Grid) String() (str string) {
	for w := grid.W().Min; w <= grid.W().Max; w++ {
		for z := grid.Z().Min; z <= grid.Z().Max; z++ {
			str += fmt.Sprintf("z=%d, w=%d\n", z, w)
			for y := grid.Y().Min; y <= grid.Y().Max; y++ {
				for x := grid.X().Min; x <= grid.X().Max; x++ {
					if cell, ok := grid.Cells[Position{X: x, Y: y, Z: z, W: w}]; ok {
						str += cell.String()
					} else {
						str += "."
					}
				}
				str += "\n"
			}
			str += "\n"
		}
	}
	return
}

func (grid Grid) Equals(other Grid) bool {
	for position, cell := range grid.Cells {
		if otherCell, ok := other.Cells[position]; !ok || otherCell != cell {
			return false
		}
	}

	return true
}

func (grid Grid) CountOccupied() (count int) {
	for _, cell := range grid.Cells {
		if cell == active {
			count++
		}
	}

	return
}

func (position Position) String() string {
	return fmt.Sprintf("[%d, %d, %d]", position.X, position.Y, position.Z)
}

func (grid Grid) Iterate(cellIteration func(cell Cell, grid Grid, position Position) Cell) Grid {
	nextGrid := EmptyGrid()
	for w := grid.W().Min - 1; w <= grid.W().Max+1; w++ {
		for z := grid.Z().Min - 1; z <= grid.Z().Max+1; z++ {
			for y := grid.Y().Min - 1; y <= grid.Y().Max+1; y++ {
				for x := grid.X().Min - 1; x <= grid.X().Max+1; x++ {
					var newCell Cell
					position := Position{X: x, Y: y, Z: z, W: w}
					if cell, ok := grid.Cells[position]; ok {
						newCell = cellIteration(cell, grid, position)
					} else {
						newCell = cellIteration(InactiveCell(), grid, position)
					}
					if newCell.IsActive() {
						nextGrid.Cells[position] = newCell
					}
				}
			}
		}
	}

	return nextGrid
}
