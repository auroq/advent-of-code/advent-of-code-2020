package pocket_test

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/challenges/day17/part2/pocket"
	"strconv"
	"strings"
	"testing"
)

func TestNewLayout(t *testing.T) {
	var tests = []struct {
		lines         []string
		expectedSeats map[pocket.Position]pocket.Cell
	}{
		{
			lines: []string{
				"###",
				"###",
				"###",
			},
			expectedSeats: map[pocket.Position]pocket.Cell{
				pocket.Position{X: 0, Y: 0, Z: 0, W: 0}: pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 0, Z: 0, W: 0}: pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 0, Z: 0, W: 0}: pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 1, Z: 0, W: 0}: pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 1, Z: 0, W: 0}: pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 1, Z: 0, W: 0}: pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 2, Z: 0, W: 0}: pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 2, Z: 0, W: 0}: pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 2, Z: 0, W: 0}: pocket.ActiveCell(),
			},
		},
		{
			lines: []string{
				"...",
				".#.",
				"...",
			},
			expectedSeats: map[pocket.Position]pocket.Cell{
				pocket.Position{X: 1, Y: 1, Z: 0, W: 0}: pocket.ActiveCell(),
			},
		},
		{
			lines: []string{
				"...",
				"...",
				"...",
			},
			expectedSeats: map[pocket.Position]pocket.Cell{},
		},
	}
	for i, test := range tests {
		test := test
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			t.Parallel()
			actual := pocket.NewGrid(test.lines)
			if len(actual.Cells) != len(test.expectedSeats) {
				t.Fatalf("length expected: %d, actual: %d", len(test.expectedSeats), len(actual.Cells))
			}
			for position, expectedSeat := range test.expectedSeats {
				if actualSeat, ok := actual.Cells[position]; !ok || actualSeat != expectedSeat {
					t.Errorf("expected position: %s not found in actual", position)
				} else if actualSeat != expectedSeat {
					t.Fatalf("position: %s, expected: %s, actual: %s", position, expectedSeat, actualSeat)
				}
			}
		})
	}
}

func TestLayout_String(t *testing.T) {
	var tests = []struct {
		startingSeats map[pocket.Position]pocket.Cell
		expected      []string
	}{
		{
			map[pocket.Position]pocket.Cell{
				pocket.Position{X: 1, Y: 0, Z: 0, W: 0}: pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 1, Z: 0, W: 0}: pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 2, Z: 0, W: 0}: pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 2, Z: 0, W: 0}: pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 2, Z: 0, W: 0}: pocket.ActiveCell(),
			},
			[]string{
				"z=0, w=0",
				".#.",
				"..#",
				"###",
			}},
		{
			map[pocket.Position]pocket.Cell{
				pocket.Position{X: 0, Y: 1, Z: -1, W: 0}: pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 2, Z: -1, W: 0}: pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 1, Z: 0, W: 0}:  pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 1, Z: 0, W: 0}:  pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 2, Z: 0, W: 0}:  pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 2, Z: 0, W: 0}:  pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 1, Z: 1, W: 0}:  pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 2, Z: 1, W: 0}:  pocket.ActiveCell(),
			},
			[]string{
				"z=-1, w=0",
				"#..",
				".#.",
				"",
				"z=0, w=0",
				"#.#",
				".##",
				"",
				"z=1, w=0",
				"#..",
				".#.",
			},
		},
		{
			map[pocket.Position]pocket.Cell{
				pocket.Position{X: 0, Y: 1, Z: -1, W: 0}: pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 2, Z: -1, W: 0}: pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 1, Z: 0, W: 0}:  pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 1, Z: 0, W: 0}:  pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 2, Z: 0, W: 0}:  pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 2, Z: 0, W: 0}:  pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 1, Z: 1, W: 0}:  pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 2, Z: 1, W: 0}:  pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 1, Z: -1, W: 1}: pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 2, Z: -1, W: 1}: pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 1, Z: 0, W: 1}:  pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 1, Z: 0, W: 1}:  pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 2, Z: 0, W: 1}:  pocket.ActiveCell(),
				pocket.Position{X: 2, Y: 2, Z: 0, W: 1}:  pocket.ActiveCell(),
				pocket.Position{X: 0, Y: 1, Z: 1, W: 1}:  pocket.ActiveCell(),
				pocket.Position{X: 1, Y: 2, Z: 1, W: 1}:  pocket.ActiveCell(),
			},
			[]string{
				"z=-1, w=0",
				"#..",
				".#.",
				"",
				"z=0, w=0",
				"#.#",
				".##",
				"",
				"z=1, w=0",
				"#..",
				".#.",
				"",
				"z=-1, w=1",
				"#..",
				".#.",
				"",
				"z=0, w=1",
				"#.#",
				".##",
				"",
				"z=1, w=1",
				"#..",
				".#.",
			},
		},
	}
	for i, test := range tests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			layout := pocket.EmptyGrid()
			layout.Cells = test.startingSeats
			expected := strings.Trim(strings.Join(test.expected, "\n"), "\r\n ")
			actual := strings.Trim(layout.String(), "\r\n ")
			if actual != expected {
				t.Fatalf("expected:\n%s\n\nactual:\n%s", expected, actual)
			}
		})
	}
}
func TestIterate(t *testing.T) {
	initial := []string{
		"...",
		".#.",
		"...",
	}
	iterations := [][]string{
		{
			"z=-1, w=-1",
			"###",
			"###",
			"###",
			"",
			"z=0, w=-1",
			"###",
			"###",
			"###",
			"",
			"z=1, w=-1",
			"###",
			"###",
			"###",
			"",
			"z=-1, w=0",
			"###",
			"###",
			"###",
			"",
			"z=0, w=0",
			"###",
			"###",
			"###",
			"",
			"z=1, w=0",
			"###",
			"###",
			"###",
			"",
			"z=-1, w=1",
			"###",
			"###",
			"###",
			"",
			"z=0, w=1",
			"###",
			"###",
			"###",
			"",
			"z=1, w=1",
			"###",
			"###",
			"###",
		},
		{
			"z=-2, w=-2",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=-1, w=-2",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=0, w=-2",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=1, w=-2",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=2, w=-2",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=-2, w=-1",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=-1, w=-1",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=0, w=-1",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=1, w=-1",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=2, w=-1",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=-2, w=0",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=-1, w=0",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=0, w=0",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=1, w=0",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=2, w=0",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=-2, w=1",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=-1, w=1",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=0, w=1",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=1, w=1",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=2, w=1",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=-2, w=2",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=-1, w=2",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=0, w=2",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=1, w=2",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
			"z=2, w=2",
			"#####",
			"#####",
			"#####",
			"#####",
			"#####",
			"",
		},
	}
	grid := pocket.NewGrid(initial)
	for i, iteration := range iterations {
		expected := strings.Join(iteration, "\n")
		grid = grid.Iterate(func(cell pocket.Cell, grid pocket.Grid, position pocket.Position) pocket.Cell {
			return pocket.ActiveCell()
		})
		actual := grid.String()
		if strings.Trim(actual, "\r\n ") != strings.Trim(expected, "\r\n ") {
			t.Fatalf("iteration %d: expected:\n%s\n\nactual:\n%s", i, expected, actual)
		}
	}
}
