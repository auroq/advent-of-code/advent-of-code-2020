package part2

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/challenges/day17/part2/pocket"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	grid := pocket.NewGrid(input)
	for i := 0; i < 6; i++ {
		grid = grid.Iterate(iterateCell)
	}
	answer = grid.CountOccupied()

	return
}

func iterateCell(cell pocket.Cell, grid pocket.Grid, position pocket.Position) pocket.Cell {
	count := GetOccupied(grid, position)
	if cell.IsActive() {
		if count == 2 || count == 3 {
			return pocket.ActiveCell()
		} else {
			return pocket.InactiveCell()
		}
	} else {
		if count == 3 {
			return pocket.ActiveCell()
		} else {
			return pocket.InactiveCell()
		}
	}
}

func GetOccupied(grid pocket.Grid, position pocket.Position) (count int) {
	count = 0
	for w := position.W - 1; w <= position.W+1; w++ {
		for z := position.Z - 1; z <= position.Z+1; z++ {
			for y := position.Y - 1; y <= position.Y+1; y++ {
				for x := position.X - 1; x <= position.X+1; x++ {
					if x == position.X && y == position.Y && z == position.Z && w == position.W {
						continue
					}
					if cell, ok := grid.Cells[pocket.Position{X: x, Y: y, Z: z, W: w}]; ok {
						if cell.IsActive() {
							count++
						}
					}
				}
			}
		}
	}

	return
}
