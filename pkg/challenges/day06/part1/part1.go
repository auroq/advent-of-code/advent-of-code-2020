package part1

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/utilities/datastructures"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	input = append(input, "")
	var lines []string
	for _, line := range input {
		if line == "" {
			group := SetFromLines(lines)
			answer += group.Count()
			lines = []string{}
		} else {
			lines = append(lines, line)
		}
	}

	return
}

func SetFromLines(input []string) *datastructures.Set {
	questions := datastructures.NewSet()
	for _, line := range input {
		for _, question := range line {
			questions.Add(question)
		}
	}

	return questions
}
