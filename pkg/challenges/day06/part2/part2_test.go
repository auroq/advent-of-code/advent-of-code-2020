package part2

import (
	"strings"
	"testing"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{
		"abc",
		"",
		"a",
		"b",
		"c",
		"",
		"ab",
		"ac",
		"",
		"a",
		"a",
		"a",
		"a",
		"",
		"b",
	}
	expected := 6
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestGroup(t *testing.T) {
	var tests = []struct {
		input []string
		count int
	}{
		{[]string{"abc"}, 3},
		{[]string{"a", "b", "c"}, 0},
		{[]string{"ab", "ac"}, 1},
		{[]string{"a", "a", "a", "a"}, 1},
		{[]string{"b"}, 1},
	}
	for _, test := range tests {
		t.Run(strings.Join(test.input, ","), func(t *testing.T) {
			actual := SetFromLines(test.input).Count()
			expected := test.count
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}

}
