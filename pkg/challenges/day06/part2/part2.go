package part2

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/utilities/datastructures"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	input = append(input, "")
	var lines []string
	for _, line := range input {
		if line == "" {
			answer += SetFromLines(lines).Count()
			lines = []string{}
		} else {
			lines = append(lines, line)
		}
	}

	return
}

type Group struct {
	questions []rune
}

func SetFromLines(input []string) *datastructures.Set {
	questions := datastructures.NewRuneSetFromString(input[0])

	for _, question := range input[0] {
		for _, line := range input[1:] {
			otherQuestions := datastructures.NewRuneSetFromString(line)
			if !otherQuestions.Contains(question) {
				questions.Remove(question)
			}
		}
	}

	return questions
}
