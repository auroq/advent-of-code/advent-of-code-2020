package part2

import (
	"errors"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/utilities"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	values, err := utilities.AsToIs(input)
	if err != nil {
		return -1, err
	}
	firstInvalid := findInvalidValue(values, 25)
	set, err := findContiguousSet(values, firstInvalid)
	if err != nil {
		return -1, err
	}

	minVal := utilities.Min(set...)
	maxVal := utilities.Max(set...)
	answer = minVal + maxVal

	return
}

func findContiguousSet(nums []int, target int) ([]int, error) {
	for i := range nums {
		sum := 0
		for j := i; j < len(nums); j++ {
			if sum < target {
				sum += nums[j]
			} else if sum == target {
				return nums[i:j], nil
			} else {
				break
			}
		}
	}

	return nil, errors.New("not found")
}

func findInvalidValue(values []int, preamble int) int {
	for i, val := range values {
		if i < preamble {
			continue
		}
		if !isValid(values[i-preamble:i], val) {
			return val
		}
	}
	return -1
}

func isValid(values []int, val int) bool {
	for i, val1 := range values {
		for j, val2 := range values {
			if i == j {
				continue
			}
			if val1+val2 == val {
				return true
			}
		}
	}

	return false
}
