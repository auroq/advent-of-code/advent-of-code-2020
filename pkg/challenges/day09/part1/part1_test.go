package part1

import (
	"strconv"
	"testing"
)

func TestFindInvalidValue(t *testing.T) {
	var nums = []int{
		35,
		20,
		15,
		25,
		47,
		40,
		62,
		55,
		65,
		95,
		102,
		117,
		150,
		182,
		127,
		219,
		299,
		277,
		309,
		576,
	}
	expected := 127
	actual := findInvalidValue(nums, 5)
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestIsValid(t *testing.T) {
	var nums []int
	for i := 1; i <= 25; i++ {
		nums = append(nums, i)
	}
	var tests = []struct {
		num   int
		valid bool
	}{
		{26, true},
		{49, true},
		{100, false},
		{50, false},
	}
	for _, test := range tests {
		t.Run(strconv.Itoa(test.num), func(t *testing.T) {
			actual := isValid(nums, test.num)
			expected := test.valid
			if actual != expected {
				t.Fatalf("expected: %t, actual: %t", expected, actual)
			}
		})
	}
}
