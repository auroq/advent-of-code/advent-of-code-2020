package part1

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2020/pkg/utilities"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	values, err := utilities.AsToIs(input)
	if err != nil {
		return -1, err
	}
	answer = findInvalidValue(values, 25)
	return
}

func findInvalidValue(values []int, preamble int) int {
	for i, val := range values {
		if i < preamble {
			continue
		}
		if !isValid(values[i-preamble:i], val) {
			return val
		}
	}
	return -1
}

func isValid(values []int, val int) bool {
	for i, val1 := range values {
		for j, val2 := range values {
			if i == j {
				continue
			}
			if val1+val2 == val {
				return true
			}
		}
	}

	return false
}
