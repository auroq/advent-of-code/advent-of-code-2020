GREEN := "\033[0;32m"
NC := "\033[0;0m"

PROJECT_DIR ?= .

DAY ?= 1
PART ?= 1

LOG := @sh -c '\
	   printf ${GREEN}; \
	   echo -e "\n> $$1\n"; \
	   printf ${NC}' VALUE


clean:
	@rm -f ${PROJECT_DIR}/aoc
	@rm -rf inputs/

format:
	$(LOG) "Formatting"
	@go fmt $$(go list ./... | grep -v /vendor/)
	$(LOG) "Vetting"
	@go vet $$(go list ./... | grep -v /vendor/)

test:
	$(LOG) "Running all tests"
	@go test $$(go list ./... | grep -v /vendor/)

test-race:
	$(LOG) "Running all tests with race detection"
	@go test -race $$(go list ./... | grep -v /vendor/)

build:
	$(LOG) "Building all files with race detection"
	@go build -race -ldflags "-extldflags '-static'" $$(go list ./... | grep -v /vendor/)

publish:
	$(LOG) "Publishing build to ${PROJECT_DIR/aoc}"
	@go build -race -ldflags "-extldflags '-static'" -o ${PROJECT_DIR}/aoc ${PROJECT_DIR}/cmd/aoc/main.go

run:
	$(LOG) "Running Day ${DAY} Part ${PART}"
	@go run ${PROJECT_DIR}/cmd/aoc/main.go -day=${DAY} -part=${PART}

submit:
	$(LOG) "Running and submitting Day ${DAY} Part ${PART}"
	@go run ${PROJECT_DIR}/cmd/aoc/main.go -day=${DAY} -part=${PART} -submit

init:
	$(LOG) "Creating new Day ${DAY} PART ${PART} from template"
	@day=$$(printf 'day%02d' ${DAY}); \
		mkdir -p ${PROJECT_DIR}/pkg/challenges/$${day}/part${PART} && \
		envsubst < templates/part.template > ${PROJECT_DIR}/pkg/challenges/$${day}/part${PART}/part${PART}.go && \
		envsubst < templates/test.template > ${PROJECT_DIR}/pkg/challenges/$${day}/part${PART}/part${PART}_test.go

part2:
	$(LOG) "Duplicated Day ${DAY} Part 1 work to Part 2"
	@day=$$(printf 'day%02d' ${DAY}); cp ${PROJECT_DIR}/pkg/challenges/$${day}/part1/part1.go ${PROJECT_DIR}/pkg/challenges/$${day}/part2/part2.go
	@day=$$(printf 'day%02d' ${DAY}); cp ${PROJECT_DIR}/pkg/challenges/$${day}/part1/part1_test.go ${PROJECT_DIR}/pkg/challenges/$${day}/part2/part2_test.go
	@day=$$(printf 'day%02d' ${DAY}); sed -i 's/part1/part2/g' ${PROJECT_DIR}/pkg/challenges/$${day}/part2/part2.go
	@day=$$(printf 'day%02d' ${DAY}); sed -i 's/Part1/Part2/g' ${PROJECT_DIR}/pkg/challenges/$${day}/part2/part2.go
	@day=$$(printf 'day%02d' ${DAY}); sed -i 's/part1/part2/g' ${PROJECT_DIR}/pkg/challenges/$${day}/part2/part2_test.go
	@day=$$(printf 'day%02d' ${DAY}); sed -i 's/Part1/Part2/g' ${PROJECT_DIR}/pkg/challenges/$${day}/part2/part2_test.go
